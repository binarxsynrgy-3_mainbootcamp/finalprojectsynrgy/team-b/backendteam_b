# Asakarya by Team B(erbalik)
Asakarya is a crowdfunding platform to support the creative industry sector in Indonesia.

## How to start
Type the following command from the root directory of the project to run the application:
```
mvn spring-boot:run
```

## Database
This project implements one-to-one, one-to-many, and many-to-many relationship types with PostgreSQL RDBMS. here's what the [ERD](https://app.diagrams.net/#G1-DsfSqyKm3LMrefjOVE_7pEv2x3drvqU) looks like:
![ERD](https://i.ibb.co/sKP8XVF/asakarya-ERD-drawio.png)

## API Documentation
Postman: https://documenter.getpostman.com/view/3237950/UVByH9Us

## Deployment
- Staging: https://asakarya-testing.herokuapp.com/
- Production: https://asakarya-api.herokuapp.com/

## Collaborators
- [Muhammad Haekal](https://gitlab.com/hklsirait)
- [Himayatul Millah](https://gitlab.com/himayatulmillah)
