package com.berbalik.asakarya;

import com.berbalik.asakarya.config.filehandler.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties({FileStorageProperties.class})
@SpringBootApplication
public class AsakaryaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsakaryaApplication.class, args);
	}

}
