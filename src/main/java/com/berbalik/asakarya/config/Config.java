package com.berbalik.asakarya.config;

import lombok.Data;

@Data
public class Config {

    public String succesCode = "200";

    public String succesMessage = "Succes";

    public String serverErrorCode = "500";

    public String createdCode = "201";

    public String createdMessage = "Created data successfully";

    public String updatedMessage = "Updated data successfully";

    public String noContentCode = "204";

    public String deletedMessage = "Data has been deleted";

    public String badRequestCode = "400";

    public String badCredentialMessage = "Bad credentials";

    public String notFoundCode = "404";

    public String notFoundMessage = "Not Found";

    public String forbiddenCode = "403";

    public String forbiddenMessage = "You don't have permission to access this service.";

    public String code = "code";

    public String message = "message";

}
