package com.berbalik.asakarya.config.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(securedEnabled = true)
public class Oauth2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    /**
     * Manage resource server.
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        super.configure(resources);
    }
    /**
     * Manage endpoints.
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and()
                .csrf()
                .disable()
                .antMatcher("/**") // block all endpoints
                .authorizeRequests()
                .antMatchers("/",
                        "/file/upload", "/file/upload-multiple", "/file/show/**",
                        "/user/register", "/user/login", "/user/detail/**",
                        "/creator/detail/**",
                        "/category/all",
                        "/campaign/all", "/campaign/detail/**", "/campaign/creator/**",
                        "/donation/**",
                        "/reward-type/all",
                        "/reward/all**", "/reward/detail/**",
                        "/contact/add",
                        "/history/all**", "/history/detail/**",
                        "/faq/all**", "/faq/detail/**",
                        "/gallery/all**", "/gallery/detail/**", "/gallery/creator/**",
                        "/admin/user/register")
                .permitAll() // allow endpoint without token
                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll()
        ;
    }

}