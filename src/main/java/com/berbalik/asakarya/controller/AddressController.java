package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.AddressModel;
import com.berbalik.asakarya.service.UserValidationService;
import com.berbalik.asakarya.service.interfaces.AddressService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private UserValidationService userValidationService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = addressService.getAllByUserId(userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map map = addressService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/main")
    public ResponseEntity<Map> getMain(Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = addressService.getMain(userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    public ResponseEntity<Map> save(@Valid @RequestBody AddressModel addressModel, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = addressService.insert(addressModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody AddressModel addressModel, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = addressService.update(addressModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/main/{id}")
    public ResponseEntity<Map> setMainAddress(@PathVariable(value = "id") Long id, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = addressService.setMainAddress(id, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = addressService.delete(id, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }
}
