package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.BookmarkModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.UserValidationService;
import com.berbalik.asakarya.service.interfaces.BookmarkService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/bookmark")
public class BookmarkController {

    @Autowired
    private BookmarkService bookmarkService;

    @Autowired
    private UserValidationService userValidationService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(
            Principal principal,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = bookmarkService.getAllByUserLoginId(userLoginId, pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    public ResponseEntity<Map> save(@Valid @RequestBody BookmarkModel bookmarkModel, Principal principal){
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = bookmarkService.insert(bookmarkModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id){
        Map map = bookmarkService.delete(id);
        return responseEntityTemplate.showResponse(map);
    }
}
