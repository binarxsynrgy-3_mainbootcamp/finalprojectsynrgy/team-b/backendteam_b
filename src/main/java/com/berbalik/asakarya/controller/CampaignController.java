package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.CampaignModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.UserValidationService;
import com.berbalik.asakarya.service.interfaces.CampaignService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/campaign")
public class CampaignController {

    @Autowired
    private CampaignService service;

    @Autowired
    UserValidationService userValidationService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @PostMapping("/add")
    @Secured({"ROLE_CREATOR"})
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map> save(@Valid @RequestBody CampaignModel campaignModel, Principal principal) throws RuntimeException{
       Long userLoginId = userValidationService.getUserLoginId(principal);
       Map map = service.insert(campaignModel, userLoginId);
       return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> update(@Valid @RequestBody CampaignModel campaignModel, Principal principal){
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = service.update(campaignModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType,
            @RequestParam(required = false) Long categoryId,
            @RequestParam(required = false) String title
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = service.getAll(pageableModel, categoryId, title);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/creator/{creatorId}")
    public ResponseEntity<Map> getAllByCreatorId(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType,
            @PathVariable(value = "creatorId") Long creatorId
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = service.getAllByCreatorId(creatorId, pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/creator/monitoring")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> getAllMonitoring(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType,
            Principal principal
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Long userLoginId = userValidationService.getUserLoginId(principal);

        Map map = service.getAllMonitoringByCreatorId(pageableModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id){
        Map map = service.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = service.delete(id, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }
}
