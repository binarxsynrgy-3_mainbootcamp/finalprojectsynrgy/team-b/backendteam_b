package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.entity.Category;
import com.berbalik.asakarya.service.interfaces.CategoryService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAll() {
        Map map = categoryService.getAll();
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> save(@Valid @RequestBody Category category) {
        Map map = categoryService.insert(category);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> update(@Valid @RequestBody Category category) {
        Map map = categoryService.update(category);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map map = categoryService.delete(id);
        return responseEntityTemplate.showResponse(map);
    }
}
