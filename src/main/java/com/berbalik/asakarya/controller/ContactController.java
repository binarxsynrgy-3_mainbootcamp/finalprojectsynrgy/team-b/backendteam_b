package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.entity.Contact;
import com.berbalik.asakarya.service.interfaces.ContactService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/contact")
public class ContactController {

    @Autowired
    private ContactService contactService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> getAll() {
        Map map = contactService.getAll();
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map map = contactService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    public ResponseEntity<Map> save(@Valid @RequestBody Contact contact) {
        Map map = contactService.insert(contact);
        return responseEntityTemplate.showResponse(map);
    }
}
