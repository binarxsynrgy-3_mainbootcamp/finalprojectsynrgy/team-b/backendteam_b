package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.CreatorModel;
import com.berbalik.asakarya.service.UserValidationService;
import com.berbalik.asakarya.service.interfaces.CreatorService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/creator")
public class CreatorController {

    @Autowired
    private CreatorService creatorService;

    @Autowired
    UserValidationService userValidationService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @PostMapping("/register")
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map> save(@Valid @RequestBody CreatorModel creatorModel, Principal principal) throws RuntimeException {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = creatorService.insert(creatorModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> update(@Valid @RequestBody CreatorModel creatorModel, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = creatorService.update(creatorModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map map = creatorService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> getById(Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = creatorService.getById(userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = creatorService.delete(id, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }
}
