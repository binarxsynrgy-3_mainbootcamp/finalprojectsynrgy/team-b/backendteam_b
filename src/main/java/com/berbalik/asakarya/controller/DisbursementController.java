package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.interfaces.DisbursementService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/disbursement")
@Secured({"ROLE_CREATOR", "ROLE_ADMIN"})
public class DisbursementController {

    @Autowired
    private DisbursementService disbursementService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAllByCampaignId(
            @RequestParam Long campaignId,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ) {
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);
        Map map = disbursementService.getAllByCampaignId(pageableModel, campaignId);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map map = disbursementService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }
}
