package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.DonationModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.UserValidationService;
import com.berbalik.asakarya.service.interfaces.DonationService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/donation")
public class DonationController {

    @Autowired
    private DonationService donationService;

    @Autowired
    private UserValidationService userValidationService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(
            @RequestParam(required = false) Long campaignId,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ) {
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = donationService.getAll(null, campaignId, pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/all/user")
    @Secured({"ROLE_INVESTOR"})
    public ResponseEntity<Map> getAllByUser(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType,
            Principal principal
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Long userLoginId = userValidationService.getUserLoginId(principal);

        Map map = donationService.getAll(userLoginId, null, pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map map = donationService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    public ResponseEntity<Map> save(@Valid @RequestBody DonationModel donationModel, Principal principal) {
        Long userLoginId = (principal == null) ? null : userValidationService.getUserLoginId(principal);

        Map map = donationService.insert(donationModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody DonationModel donationModel, Principal principal){
        Long userLoginId = (principal == null) ? null : userValidationService.getUserLoginId(principal);

        Map map = donationService.update(donationModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }
}
