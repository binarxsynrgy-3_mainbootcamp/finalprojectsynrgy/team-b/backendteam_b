package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.interfaces.DonationRewardService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/donation-reward")
public class DonationRewardController {

    @Autowired
    private DonationRewardService donationRewardService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> getAll(
            @RequestParam Long campaignId,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = donationRewardService.getAllByCampaignId(campaignId, pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id){
        Map map = donationRewardService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/deliver/{id}")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> deliver(@PathVariable(value = "id") Long id){
        Map map = donationRewardService.deliver(id);
        return responseEntityTemplate.showResponse(map);
    }
}
