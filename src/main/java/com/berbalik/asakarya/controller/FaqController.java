package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.FaqModel;
import com.berbalik.asakarya.service.interfaces.FaqService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/faq")
public class FaqController {

    @Autowired
    private FaqService faqService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAllByCampaignId(@RequestParam Long campaignId){
        Map map = faqService.getAllByCampaignId(campaignId);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id){
        Map map = faqService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> save(@Valid @RequestBody FaqModel faqModel){
        Map map = faqService.insert(faqModel);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> update(@Valid @RequestBody FaqModel faqModel){
        Map map = faqService.update(faqModel);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id){
        Map map = faqService.delete(id);
        return responseEntityTemplate.showResponse(map);
    }
}
