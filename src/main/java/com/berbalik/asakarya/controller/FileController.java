package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.config.filehandler.FileStorageException;
import com.berbalik.asakarya.dto.FileModel;
import com.berbalik.asakarya.service.FileStorageService;
import com.berbalik.asakarya.util.DateFormatter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/file")
public class FileController {

    @Value("${app.uploadto.cdn}")
    private String UPLOADED_FOLDER;

    @Autowired
    private FileStorageService fileStorageService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = {"multipart/form-data", "application/json"})
    public FileModel uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        String strDate = DateFormatter.SimpleDateFormatter();

        String fileNameForDownload = strDate + file.getOriginalFilename();
        String fileName = UPLOADED_FOLDER + fileNameForDownload;

        Path TO = Paths.get(fileName);

        // file type validation
        if (!(file.getContentType().equals("image/png") || file.getContentType().equals("image/jpg") || file.getContentType().equals("image/jpeg") || file.getContentType().equals("application/pdf"))) {
            throw new FileStorageException("Content type not allowed: " + file.getContentType());
        }

        try {
            log.info("Upload file {}", fileNameForDownload);
            Files.copy(file.getInputStream(), TO);
        } catch (Exception e) {
            e.printStackTrace();
            return new FileModel(
                    fileName,
                    null,
                    file.getContentType(),
                    file.getSize(),
                    e.getMessage());
        }

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("file/show/")
                .path(fileNameForDownload)
                .toUriString();

        return new FileModel(
                fileNameForDownload,
                fileDownloadUri,
                file.getContentType(),
                file.getSize(),
                "false");
    }

    @GetMapping("show/{fileName:.+}")
    public ResponseEntity<Resource> showFile(@PathVariable String fileName, HttpServletRequest request) {
        Resource resource = fileStorageService.loadFileAsResource(fileName);
        String contentType = null;
        try {
            System.out.println("resource.getFile().getAbsolutePath===" + resource.getFile().getAbsolutePath());
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException e) {
            log.warn("Could not determine file type");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        System.out.println("filename=2=" + HttpHeaders.CONTENT_DISPOSITION);
        System.out.println("filename=3=" + resource.getFilename());
        System.out.println("filename=3=" + resource);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment: filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @PostMapping("/upload-multiple")
    public List<FileModel> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) throws IOException {
        return Arrays.asList(files)
                .stream()
                .map(file -> {
                    try {
                        return uploadFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    private File multipartToFile(MultipartFile upload, String routeName) {
        String base = "";

        log.info(String.format("Trying upload file: %s", upload.getOriginalFilename()));
        File file = new File(base + upload.getOriginalFilename());

        try {
            log.info(String.format("Saving uploaded file to: %s", file.getAbsolutePath()));
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(upload.getBytes());
            fos.close();
        } catch (IOException e) {
            log.error(String.format("Error: POST|UPLOAD %s", routeName), e);
        }

        return file;
    }

    private File multipartToFile(MultipartFile upload) {
        return multipartToFile(upload, UPLOADED_FOLDER);
    }

}
