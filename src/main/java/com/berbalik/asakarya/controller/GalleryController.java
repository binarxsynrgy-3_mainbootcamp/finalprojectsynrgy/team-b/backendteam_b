package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.CampaignModel;
import com.berbalik.asakarya.dto.GalleryModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.UserValidationService;
import com.berbalik.asakarya.service.interfaces.GalleryService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/gallery")
public class GalleryController {

    @Autowired
    private GalleryService galleryService;

    @Autowired
    UserValidationService userValidationService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType,
            @RequestParam(required = false) Long categoryId,
            @RequestParam(required = false) String title
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = galleryService.getAll(pageableModel, categoryId, title);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/creator/{creatorId}")
    public ResponseEntity<Map> getAllByCreatorId(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType,
            @PathVariable(value = "creatorId") Long creatorId
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = galleryService.getAllByCreatorId(creatorId, pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id){
        Map map = galleryService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    @Secured({"ROLE_CREATOR"})
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map> save(@Valid @RequestBody GalleryModel galleryModel, Principal principal) throws RuntimeException{
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = galleryService.insert(galleryModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> update(@Valid @RequestBody GalleryModel galleryModel, Principal principal){
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = galleryService.update(galleryModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = galleryService.delete(id, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }
}
