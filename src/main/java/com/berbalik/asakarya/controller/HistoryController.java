package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.HistoryModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.interfaces.HistoryService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/history")
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAllByCampaignId(
            @RequestParam Long campaignId,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ){
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = historyService.getAllByCampaignId(campaignId, pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map map = historyService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> save(@Valid @RequestBody HistoryModel historyModel) {
        Map map = historyService.insert(historyModel);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> update(@Valid @RequestBody HistoryModel historyModel) {
        Map map = historyService.update(historyModel);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map map = historyService.delete(id);
        return responseEntityTemplate.showResponse(map);
    }
}
