package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.RewardModel;
import com.berbalik.asakarya.service.interfaces.RewardService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/reward")
public class RewardController {

    @Autowired
    private RewardService rewardService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAllByCampaignId(@RequestParam Long campaignId) {
        Map map = rewardService.getAllByCampaignId(campaignId);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id){
        Map map = rewardService.getById(id);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> update(@Valid @RequestBody List<RewardModel> rewardModels){
        Map map = rewardService.update(rewardModels);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/campaign/{id}")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id){
        Map map = rewardService.deleteAllByCampaignId(id);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    @Secured({"ROLE_CREATOR"})
    public ResponseEntity<Map> save(@Valid @RequestBody List<RewardModel> rewardModels) {
        Map map = rewardService.insert(rewardModels);
        return responseEntityTemplate.showResponse(map);
    }
}
