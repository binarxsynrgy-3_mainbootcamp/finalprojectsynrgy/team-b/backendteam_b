package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.service.interfaces.RewardTypeService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/reward-type")
public class RewardTypeController {

    @Autowired
    private RewardTypeService rewardTypeService;

    private ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAll() {
        Map map = rewardTypeService.getAll();
        return responseEntityTemplate.showResponse(map);
    }
}
