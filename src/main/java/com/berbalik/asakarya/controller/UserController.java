package com.berbalik.asakarya.controller;

import com.berbalik.asakarya.dto.UpdatePasswordModel;
import com.berbalik.asakarya.dto.UserModel;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.UserValidationService;
import com.berbalik.asakarya.service.interfaces.UserService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Value("${expired.token.password.minute:}")//FILE_SHOW_RUL
    private int expiredToken;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserValidationService userValidationService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @PostMapping("/register")
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map> save(@Valid @RequestBody UserModel userModel) throws RuntimeException{
        Map map = userService.register(userModel, false);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/login")
    public ResponseEntity<Map> login(HttpServletRequest request, @Valid @RequestBody UserModel userModel) {
        String baseUrl = ServletUriComponentsBuilder.fromRequestUri(request)
                .replacePath(null)
                .build()
                .toUriString();
        System.out.println(baseUrl);
        Map map = userService.login(baseUrl, userModel);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody UserModel userModel, Principal principal){
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = userService.update(userModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/detail")
    public ResponseEntity<Map> getById(Principal principal){
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = userService.getById(userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Map> delete(Principal principal){
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = userService.delete(userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update-password")
    public ResponseEntity<Map> updatePassword(@Valid @RequestBody UpdatePasswordModel updatePasswordModel, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);
        Map map = userService.updatePassword(updatePasswordModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }
}
