package com.berbalik.asakarya.controller.admin;

import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.interfaces.CreatorService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/admin/creator")
@Secured({"ROLE_ADMIN"})
public class AdmCreatorController {

    @Autowired
    private CreatorService creatorService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ) {
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = creatorService.getAll(pageableModel);
        return responseEntityTemplate.showResponse(map);
    }
}
