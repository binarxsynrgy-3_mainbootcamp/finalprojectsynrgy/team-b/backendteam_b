package com.berbalik.asakarya.controller.admin;

import com.berbalik.asakarya.service.interfaces.DashboardService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/admin")
@Secured({"ROLE_ADMIN"})
public class AdmDashboardController {

    @Autowired
    private DashboardService dashboardService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/dashboard")
    public ResponseEntity<Map> sumDonation() {
        Map map = dashboardService.dashboard();
        return responseEntityTemplate.showResponse(map);
    }
}
