package com.berbalik.asakarya.controller.admin;

import com.berbalik.asakarya.dto.DisbursementModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.UserValidationService;
import com.berbalik.asakarya.service.interfaces.DisbursementService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RestController
@RequestMapping("/admin/disbursement")
@Secured({"ROLE_ADMIN"})
public class AdmDisbursementController {

    @Autowired
    UserValidationService userValidationService;

    @Autowired
    private DisbursementService disbursementService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAllByCampaignId(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ) {
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);
        Map map = disbursementService.adminGetAll(pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @PostMapping("/add")
    public ResponseEntity<Map> save(@Valid @RequestBody DisbursementModel disbursementModel, Principal principal) {
        Long userLoginId = userValidationService.getUserLoginId(principal);

        Map map = disbursementService.insert(disbursementModel, userLoginId);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody DisbursementModel disbursementModel) {
        Map map = disbursementService.update(disbursementModel);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map map = disbursementService.delete(id);
        return responseEntityTemplate.showResponse(map);
    }
}
