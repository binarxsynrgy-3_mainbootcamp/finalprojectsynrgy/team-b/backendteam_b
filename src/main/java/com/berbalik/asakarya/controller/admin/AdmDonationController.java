package com.berbalik.asakarya.controller.admin;

import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.service.interfaces.DonationService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/admin/donation")
@Secured({"ROLE_ADMIN"})
public class AdmDonationController {

    @Autowired
    private DonationService donationService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(
            @RequestParam(required = false) Long campaignId,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ) {
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = donationService.adminGetAll(campaignId, pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/verify/{id}")
    public ResponseEntity<Map> verify(@PathVariable(value = "id") Long id, @RequestParam boolean isVerified){
        Map map = donationService.verify(id, isVerified);
        return responseEntityTemplate.showResponse(map);
    }
}
