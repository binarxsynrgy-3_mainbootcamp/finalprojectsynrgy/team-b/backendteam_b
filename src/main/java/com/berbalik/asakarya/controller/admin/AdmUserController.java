package com.berbalik.asakarya.controller.admin;

import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.dto.UserModel;
import com.berbalik.asakarya.service.interfaces.UserService;
import com.berbalik.asakarya.util.ResponseEntityTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/admin/user")
public class AdmUserController {

    @Autowired
    private UserService userService;

    ResponseEntityTemplate responseEntityTemplate = new ResponseEntityTemplate();

    @PostMapping("/register")
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map> save(@Valid @RequestBody UserModel userModel) throws RuntimeException{
        Map map = userService.register(userModel, true);
        return responseEntityTemplate.showResponse(map);
    }

    @GetMapping("/all")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> getAll(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortType
    ) {
        PageableModel pageableModel = new PageableModel(page, size, sortBy, sortType);

        Map map = userService.getAll(pageableModel);
        return responseEntityTemplate.showResponse(map);
    }

    @PutMapping("/reset-password")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> resetPassword(@Valid @RequestBody UserModel userModel) {
        Map map = userService.resetPassword(userModel);
        return responseEntityTemplate.showResponse(map);
    }

    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id){
        Map map = userService.delete(id);
        return responseEntityTemplate.showResponse(map);
    }
}
