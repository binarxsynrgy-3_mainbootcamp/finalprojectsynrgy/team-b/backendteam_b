package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class AddressModel {

    private Long id;
    private String recipient;
    private String phone;
    private String address;
    private Integer postalCode;
    private boolean main;
    private Long userId;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
}
