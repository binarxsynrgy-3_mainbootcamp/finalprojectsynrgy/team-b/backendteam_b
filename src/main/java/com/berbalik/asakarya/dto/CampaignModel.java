package com.berbalik.asakarya.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class CampaignModel {

    Long id;
    String title;
    String description;
    float fundAmount;
    LocalDate due;
    String imgUrl;
    String website;
    String location;
    Long daysLeft;
    float sumDonation;
    int countDonation;
    int status;
    float sumDisbursement;
    Long categoryId;
    String categoryName;
    Long creatorId;
    String creatorOrganization;
    String profileFullName;
    String profileImage;
    Date createdAt;
    Date updatedAt;
    Date deletedAt;
}
