package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ContactModel {

    private Long id;
    private String name;
    private String email;
    private String message;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
}
