package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CreatorModel {

    Long id;
    String organization;
    String occupation;
    String socialMedia;
    String socialMediaAccount;
    String bio;
    String nik;
    String ktpUrl;
    String bankName;
    String bankAccount;
    String bankAccountName;
    Date createdAt;
    Date updatedAt;
    Date deletedAt;
}
