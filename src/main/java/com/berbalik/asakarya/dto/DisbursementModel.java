package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class DisbursementModel {

    Long id;
    String title;
    float amount;
    String description;
    String paymentSlipUrl;
    Long userId;
    String adminFullName;
    String adminProfileImage;
    Long campaignId;
    Date createdAt;
    Date updatedAt;
    Date deletedAt;
}
