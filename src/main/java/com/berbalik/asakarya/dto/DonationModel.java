package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class DonationModel {

    private Long id;
    private float amount;
//    private String bankName;
//    private String bankAccount;
    private String paymentSlipUrl;
    private String notes;
    private int status;
    private Long addressId;
    private Long userId;
    private String profileFullName;
    private String profileImage;
    private Long campaignId;
    private String campaignTitle;
    private String campaignImage;
    private String campaignCategoryName;
    private String campaignCreatorName;
    private Long rewardId;
    private Character rewardType;
    private String rewardItem;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
}
