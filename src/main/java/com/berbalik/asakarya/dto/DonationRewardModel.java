package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class DonationRewardModel {

    private Long id;
    private Long addressId;
    private String recipient;
    private String phone;
    private String address;
    private Integer postalCode;
    private boolean main;
    private Long userId;
    private String profileFullName;
    private String profileImage;
    private Long campaignId;
    private Long rewardId;
    private Character rewardType;
    private String rewardItem;
    private String rewardImage;
    private boolean delivered;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
}
