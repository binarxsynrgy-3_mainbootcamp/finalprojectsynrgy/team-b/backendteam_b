package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class FaqModel {

    private Long id;
    private String question;
    private String answer;
    private Long campaignId;
    private Long creatorId;
    private String profileFullName;
    private String profileImage;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
}
