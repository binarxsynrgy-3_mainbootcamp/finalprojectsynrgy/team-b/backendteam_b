package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class GalleryModel {

    Long id;
    String title;
    String description;
    String website;
    String imgUrl1;
    String imgUrl2;
    String imgUrl3;
    boolean promoted;
    Long creatorId;
    String creatorSocialMedia;
    Long categoryId;
    String categoryName;
    Date createdAt;
    Date updatedAt;
    Date deletedAt;
}
