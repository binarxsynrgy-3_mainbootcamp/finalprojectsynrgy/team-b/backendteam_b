package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class HistoryModel {

    private Long id;
    private String title;
    private String activity;
    private String imgUrl;
    private Long campaignId;
    private Long creatorId;
    private String profileFullName;
    private String profileImage;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
}
