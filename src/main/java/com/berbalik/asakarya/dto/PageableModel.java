package com.berbalik.asakarya.dto;

import lombok.Data;

@Data
public class PageableModel {

    private Integer page;
    private Integer size;
    private String sortBy;
    private String sortType;

    public PageableModel(Integer page, Integer size, String sortBy, String sortType) {
        this.page = (page == null) ? 0 : page;
        this.size = (size == null) ? 20 : size;
        this.sortBy = (sortBy == null) ? "id" : sortBy;
        this.sortType = (sortType == null) ? "asc" : sortType;
    }

    public PageableModel() {
    }
}
