package com.berbalik.asakarya.dto;

import lombok.Data;

import java.util.Date;

@Data
public class RewardModel {

    private Long id;
    private Long rewardTypeId;
    private Long campaignId;
    private String item;
    private boolean limited;
    private String imgUrl;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
}
