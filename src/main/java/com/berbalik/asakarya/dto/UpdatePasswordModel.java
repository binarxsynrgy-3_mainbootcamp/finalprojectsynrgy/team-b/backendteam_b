package com.berbalik.asakarya.dto;

import lombok.Data;

@Data
public class UpdatePasswordModel {

    private String oldPassword;
    private String newPassword;
}
