package com.berbalik.asakarya.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
public class UserModel {

    Long id;
    String username;
    String fullName;
    String password;
    String phone;
    Character gender;
    LocalDate dob;
    String imgUrl;
    List<String> roles;
    Date createdAt;
    Date updatedAt;
    Date deletedAt;
}
