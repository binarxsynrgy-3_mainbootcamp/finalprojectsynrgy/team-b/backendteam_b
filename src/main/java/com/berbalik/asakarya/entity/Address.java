package com.berbalik.asakarya.entity;

import com.berbalik.asakarya.entity.oauth.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "address")
@Where(clause = "deleted_at IS NULL")
public class Address extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "recipient", length = 45)
    private String recipient;

    @Column(name = "phone", length = 13)
    private String phone;

    @Column(name = "address", columnDefinition = "TEXT")
    private String address;

    @Column(name = "postal_code")
    private Integer postalCode;

    @Column(name = "main", nullable = false)
    private boolean main = false;

    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

//    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JsonBackReference
//    private List<Donation> donations;
}
