package com.berbalik.asakarya.entity;

import com.berbalik.asakarya.entity.oauth.User;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "bookmark")
@Where(clause = "deleted_at IS NULL")
public class Bookmark extends AbstractDate implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

//    @ManyToOne(targetEntity = Campaign.class, cascade = CascadeType.ALL)
    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private Campaign campaign;
}
