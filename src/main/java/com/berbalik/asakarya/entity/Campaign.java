package com.berbalik.asakarya.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "campaign")
@Where(clause = "deleted_at IS NULL")
public class Campaign extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", length = 255, nullable = false)
    private String title;

    @Column(name = "description", columnDefinition ="TEXT")
    private String description;

    @Column(name = "fund_amount", columnDefinition = "FLOAT")
    private float fundAmount;

    @Column(name = "due")
    private LocalDate due;

    @Column(name = "img_url", columnDefinition = "TEXT")
    private String imgUrl;

    @Column(name = "website", columnDefinition = "TEXT")
    private String website;

    @Column(name = "location")
    private String location;

    @Column(name = "sum_donation", columnDefinition = "FLOAT")
    private float sumDonation = 0f;

    @Column(name = "count_donation")
    private int countDonation = 0;

    @Column(name = "status")
    private int status=0; // 0 = proses; 1 = terverifikasi; 2 = gagal

    @Column(name = "sum_disbursement", columnDefinition = "FLOAT")
    private float sumDisbursement = 0f;

//    @ManyToOne(targetEntity = Category.class, cascade = CascadeType.ALL)
    @ManyToOne
    private Category category;

//    @ManyToOne(targetEntity = Creator.class, cascade = CascadeType.ALL)
    @ManyToOne
    @JoinColumn(name = "creator_id", referencedColumnName = "user_id")
    private Creator creator;

//    @OneToMany(mappedBy = "campaign", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JsonBackReference
//    private List<Donation> donations;

//    @OneToMany(mappedBy = "campaign", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JsonBackReference
//    private List<Bookmark> bookmarks;

//    @OneToMany(mappedBy = "campaign", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JsonBackReference
//    private List<Reward> rewards;
}
