package com.berbalik.asakarya.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "contact")
@Where(clause = "deleted_at IS NULL")
public class Contact extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 60)
    private String name;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "message", columnDefinition = "TEXT")
    private String message;
}
