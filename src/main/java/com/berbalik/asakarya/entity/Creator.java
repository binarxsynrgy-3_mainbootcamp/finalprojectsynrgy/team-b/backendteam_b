package com.berbalik.asakarya.entity;

import com.berbalik.asakarya.entity.oauth.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "creator")
@Where(clause = "deleted_at IS NULL")
public class Creator extends AbstractDate implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "organization")
    private String organization;

    @Column(name = "occupation", length = 60)
    private String occupation;

    @Column(name = "social_media", length = 20)
    private String socialMedia;

    @Column(name = "social_media_account")
    private String socialMediaAccount;

    @Column(name = "bio", columnDefinition = "TEXT")
    private String bio;

    @Column(name = "nik", length = 16)
    private String nik;

    @Column(name = "ktp_url", columnDefinition = "TEXT")
    private String ktpUrl;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "bank_account", length = 120)
    private String bankAccount;

    @Column(name = "bank_account_name", length = 120)
    private String bankAccountName;

    @OneToOne
    @MapsId
    @JoinColumn(name = "user_id")
    @JsonManagedReference
    private User user;

//    @OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JsonBackReference
//    private List<Campaign> campaigns;
}
