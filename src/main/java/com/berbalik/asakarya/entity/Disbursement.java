package com.berbalik.asakarya.entity;

import com.berbalik.asakarya.entity.oauth.User;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "disbursement")
@Where(clause = "deleted_at IS NULL")
public class Disbursement extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "amount", columnDefinition = "FLOAT")
    private float amount;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "payment_slip_url", columnDefinition = "TEXT")
    private String paymentSlipUrl;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private Campaign campaign;
}
