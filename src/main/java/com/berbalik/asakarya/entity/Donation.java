package com.berbalik.asakarya.entity;

import com.berbalik.asakarya.entity.oauth.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "donation")
@Where(clause = "deleted_at IS NULL")
public class Donation extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount", nullable = false, columnDefinition = "FLOAT")
    private float amount;

//    @Column(name = "bank_name")
//    private String bankName;
//
//    @Column(name = "bank_account", length = 60)
//    private String bankAccount;

    @Column(name = "payment_slip_url", columnDefinition = "TEXT")
    private String paymentSlipUrl;

    @Column(name = "notes")
    private String notes;

    @Column(name = "status")
    private int status = 0; // 0 = proses; 1 = berhasil; 2 = gagal

    @Column(name = "delivered")
    private boolean delivered = false;

//    @ManyToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;

//    @ManyToOne(targetEntity = Campaign.class, cascade = CascadeType.ALL)
    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private Campaign campaign;

//    @ManyToOne(targetEntity = Reward.class, cascade = CascadeType.ALL)
    @ManyToOne
    @JoinColumn(name = "reward_id")
    private Reward reward;
}
