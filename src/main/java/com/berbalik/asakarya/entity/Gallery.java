package com.berbalik.asakarya.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "gallery")
@Where(clause = "deleted_at IS NULL")
public class Gallery extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "website", columnDefinition = "TEXT")
    private String website;

    @Column(name = "img_url1", columnDefinition = "TEXT")
    private String imgUrl1;

    @Column(name = "img_url2", columnDefinition = "TEXT")
    private String imgUrl2;

    @Column(name = "img_url3", columnDefinition = "TEXT")
    private String imgUrl3;

    @Column(name = "promoted")
    private boolean promoted = false;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private Creator creator;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
