package com.berbalik.asakarya.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "history")
@Where(clause = "deleted_at IS NULL")
public class History extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "activity", columnDefinition = "TEXT")
    private String activity;

    @Column(name = "img_url", columnDefinition = "TEXT")
    private String imgUrl;

//    @ManyToOne(targetEntity = Campaign.class, cascade = CascadeType.ALL)
    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private Campaign campaign;
}
