package com.berbalik.asakarya.entity;


import com.berbalik.asakarya.entity.oauth.User;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "profile")
@Where(clause = "deleted_at IS NULL")
public class Profile extends AbstractDate implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "full_name", length = 45)
    private String fullName;

    @Column(name = "phone", length = 13)
    private String phone;

    @Column(name = "gender", length = 1)
    private Character gender;

    @Column(name = "dob")
    private LocalDate dob;

    @Column(name = "img_url", columnDefinition = "TEXT")
    private String imgUrl;

    @OneToOne
    @MapsId
    @JoinColumn(name = "user_id")
    private User user;

}
