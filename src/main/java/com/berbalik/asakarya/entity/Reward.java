package com.berbalik.asakarya.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "reward")
@Where(clause = "deleted_at IS NULL")
public class Reward extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "item")
    private String item;

    @Column(name = "limited")
    private boolean limited = false;

    @Column(name = "img_url", columnDefinition = "TEXT")
    private String imgUrl;

//    @ManyToOne(targetEntity = RewardType.class, cascade = CascadeType.ALL)
    @ManyToOne
    private RewardType rewardType;

//    @ManyToOne(targetEntity = Campaign.class, cascade = CascadeType.ALL)
    @ManyToOne
    private Campaign campaign;

//    @OneToMany(mappedBy = "reward", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JsonBackReference
//    private List<Donation> donations;
}
