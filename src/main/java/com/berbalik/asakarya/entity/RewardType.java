package com.berbalik.asakarya.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "reward_type")
@Where(clause = "deleted_at IS NULL")
public class RewardType extends AbstractDate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type", length = 1, nullable = false)
    private Character type;

    @Column(name = "gte", columnDefinition = "FLOAT", nullable = false)
    private float gte = 0f;

    @Column(name = "lte", columnDefinition = "FLOAT")
    private float lte = 0f;

//    @OneToMany(mappedBy = "rewardType", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JsonIgnore
//    private List<Reward> rewards;

    public RewardType() {
    }

    public RewardType(Character type, float gte, float lte) {
        this.type = type;
        this.gte = gte;
        this.lte = lte;
    }
}
