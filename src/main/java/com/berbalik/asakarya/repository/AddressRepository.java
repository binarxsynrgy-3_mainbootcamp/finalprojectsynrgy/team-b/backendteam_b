package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long> {

    @Query(value = "SELECT * FROM address WHERE user_id = :userId AND deleted_at IS NULL", nativeQuery = true)
    public List<Address> getAllByUserId(Long userId);

    @Query(value = "SELECT * FROM address WHERE user_id = :userId AND main = true AND deleted_at IS NULL", nativeQuery = true)
    public Address getMainAddress(Long userId);
}
