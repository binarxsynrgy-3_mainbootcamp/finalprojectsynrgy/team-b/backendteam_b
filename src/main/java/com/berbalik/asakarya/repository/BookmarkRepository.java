package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Bookmark;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BookmarkRepository extends JpaRepository<Bookmark, Long> {

    @Query("SELECT b FROM Bookmark b WHERE b.user.id = :id")
    public Page<Bookmark> getAllByUserId(@Param("id") Long id, Pageable pageable);

    @Query(value = "SELECT * FROM bookmark WHERE user_id = :userId AND campaign_id = :campaignId AND deleted_at IS NULL", nativeQuery = true)
    public Bookmark findOneByUserAndCampaignId(@Param("userId") Long userId, @Param("campaignId") Long campaignId);
}
