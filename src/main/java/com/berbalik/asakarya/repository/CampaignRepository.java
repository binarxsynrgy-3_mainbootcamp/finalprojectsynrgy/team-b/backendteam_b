package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Campaign;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface CampaignRepository extends JpaRepository<Campaign, Long> {

    @Query("SELECT CASE WHEN COUNT(c)> 0 THEN true ELSE false END FROM Campaign c WHERE id = ?1 AND status = 1 AND due >= CURRENT_DATE")
    public boolean existsById(Long aLong);

    @Query("SELECT c FROM Campaign c WHERE c.id = :id")
    public Campaign getById(Long id);

    @Query("SELECT c FROM Campaign c WHERE c.status = 1 AND c.due >= CURRENT_DATE")
    public Page<Campaign> findAll(Pageable pageable);

    @Query("SELECT c FROM Campaign c WHERE c.status = 1 AND c.due >= CURRENT_DATE")
    public List<Campaign> findAll();

    @Query("SELECT c FROM Campaign c WHERE c.category.id = :categoryId AND c.status = 1 AND c.due >= CURRENT_DATE")
    public Page<Campaign> findAllByCategoryId(Long categoryId, Pageable pageable);

    @Query("SELECT c FROM Campaign c WHERE c.creator.id = :creatorId AND c.status = 1 AND c.due >= CURRENT_DATE")
    public Page<Campaign> findAllByCreatorId(Long creatorId, Pageable pageable);

    @Query("SELECT c FROM Campaign c WHERE c.creator.id = :creatorId")
    public Page<Campaign> getAllMonitoringByCreatorId(Long creatorId, Pageable pageable);

    @Query("SELECT c FROM Campaign c WHERE LOWER(c.title) LIKE :title AND c.status = 1 AND c.due >= CURRENT_DATE")
    public Page<Campaign> findAllByTitleLike(String title, Pageable pageable);

    @Query("SELECT c FROM Campaign c WHERE LOWER(c.title) LIKE :title AND c.category.id = :categoryId AND c.status = 1 AND c.due >= CURRENT_DATE")
    public Page<Campaign> findAllByCategoryIdAndTitleLike(Long categoryId, String title, Pageable pageable);

    // [ADMIN] get list campaign
    @Query("SELECT c FROM Campaign c")
    public List<Campaign> getList();

    // [ADMIN] get all campaign with pagination
    @Query("SELECT c FROM Campaign c")
    public Page<Campaign> adminFindAll(Pageable pageable);
}
