package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contact, Long> {
}
