package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Creator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CreatorRepository extends JpaRepository<Creator, Long> {

    @Query(value = "SELECT * FROM creator WHERE user_id = :id AND deleted_at IS NULL", nativeQuery = true)
    public Creator getByUserId(@Param("id") Long id);
}
