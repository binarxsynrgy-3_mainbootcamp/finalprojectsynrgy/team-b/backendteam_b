package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DashboardRepository extends JpaRepository<Campaign, Long> {

    // Dashboard
    @Query("SELECT COUNT(id) FROM Campaign c WHERE c.status = 1 AND c.due >= CURRENT_DATE")
    public Integer countActiveCampaign();

    @Query("SELECT COUNT(id) FROM Campaign c WHERE c.status = 1 AND c.due < CURRENT_DATE")
    public Integer countCampaignFinished();

    @Query("SELECT SUM(d.amount) FROM Donation d")
    public Float sumDonation();

    @Query("SELECT SUM(d.amount) FROM Donation d WHERE d.status = 1")
    public Float sumVerifiedDonation();

    @Query("SELECT COUNT(id) FROM Gallery g")
    public Integer countGallery();
}
