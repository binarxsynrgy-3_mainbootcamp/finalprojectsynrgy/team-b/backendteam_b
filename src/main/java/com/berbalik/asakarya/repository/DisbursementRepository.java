package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Disbursement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DisbursementRepository extends JpaRepository<Disbursement, Long> {

    @Query("SELECT d FROM Disbursement d WHERE d.campaign.id = :campaignId")
    public Page<Disbursement> findAllByCampaignId(Long campaignId, Pageable pageable);

}
