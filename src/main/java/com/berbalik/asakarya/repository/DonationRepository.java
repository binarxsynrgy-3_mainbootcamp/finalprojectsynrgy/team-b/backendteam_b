package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Donation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DonationRepository extends JpaRepository<Donation, Long> {

    @Query("SELECT CASE WHEN COUNT(d)> 0 THEN true ELSE false END FROM Donation d WHERE id = ?1 AND status = 1")
    boolean existsById(Long aLong);

    // get detail donation
    @Query("SELECT d FROM Donation d WHERE d.id = :id")
    public Donation getById(@Param("id") Long id);

    // get all verified donation
    @Query("SELECT d FROM Donation d WHERE status = 1")
    public Page<Donation> findAll(Pageable pageable);

    // get all donation from user with id = userId
    @Query("SELECT d FROM Donation d JOIN d.address a WHERE a.user.id = :userId")
    public Page<Donation> findAllByUserId(Long userId, Pageable pageable);

    // get all verified donation from campaign with id = campaignId
    @Query("SELECT d FROM Donation d WHERE status = 1 AND d.campaign.id = :campaignId")
    public Page<Donation> findAllByCampaignId(Long campaignId, Pageable pageable);

    @Query("Select d FROM Donation d WHERE d.status = 1 AND d.reward.id IS NOT NULL AND d.campaign.id = :campaignId")
    public Page<Donation> findAllDonationRewardByCampaignId(Long campaignId, Pageable pageable);

    // [ADMIN] get all donation
    @Query("SELECT d FROM Donation d")
    public Page<Donation> adminFindAll(Pageable pageable);

    // [ADMIN] get all donation by campaignId
    @Query("SELECT d FROM Donation d WHERE d.campaign.id = :campaignId")
    public Page<Donation> adminFindAllByCampaignId(Long campaignId, Pageable pageable);
}
