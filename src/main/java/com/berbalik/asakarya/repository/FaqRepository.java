package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Faq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FaqRepository extends JpaRepository<Faq, Long> {

    @Query(value = "SELECT * FROM faq WHERE campaign_id = :campaignId and deleted_at IS NULL", nativeQuery = true)
    public List<Faq> getAllByCampaignId(Long campaignId);
}
