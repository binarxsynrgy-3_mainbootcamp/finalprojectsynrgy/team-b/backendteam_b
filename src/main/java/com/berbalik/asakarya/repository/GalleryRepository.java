package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Gallery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GalleryRepository extends JpaRepository<Gallery, Long> {

    @Query("SELECT g FROM Gallery g WHERE g.id = :id")
    public Gallery getById(Long id);

    @Query("SELECT g FROM Gallery g WHERE g.creator.id = :creatorId")
    public Page<Gallery> findAllByCreatorId(Long creatorId, Pageable pageable);

    @Query("SELECT g FROM Gallery g WHERE g.category.id = :categoryId")
    public Page<Gallery> findAllByCategoryId(Long categoryId, Pageable pageable);

    @Query("SELECT g FROM Gallery g WHERE LOWER(g.title) LIKE :title")
    public Page<Gallery> findAllByTitleLike(String title, Pageable pageable);

    @Query("SELECT g FROM Gallery g WHERE g.category.id = :categoryId AND LOWER(g.title) LIKE :title")
    public Page<Gallery> findAllByCategoryIdAndTitleLike(Long categoryId, String title, Pageable pageable);
}
