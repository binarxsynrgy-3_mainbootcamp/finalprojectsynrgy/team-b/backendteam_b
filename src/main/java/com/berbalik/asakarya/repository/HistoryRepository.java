package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.History;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface HistoryRepository extends JpaRepository<History, Long> {

    @Query("SELECT h FROM History h WHERE h.campaign.id = :campaignId")
    public Page<History> getAllByCampaignId(Long campaignId, Pageable pageable);
}
