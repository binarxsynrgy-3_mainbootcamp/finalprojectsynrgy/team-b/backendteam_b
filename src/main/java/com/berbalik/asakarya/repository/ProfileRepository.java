package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

}
