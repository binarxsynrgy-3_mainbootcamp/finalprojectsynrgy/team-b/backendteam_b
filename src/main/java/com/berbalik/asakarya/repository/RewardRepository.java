package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.Reward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RewardRepository extends JpaRepository<Reward, Long> {

    @Query(value = "SELECT * FROM reward WHERE campaign_id = :campaignId AND deleted_at IS NULL ORDER BY reward_type_id ASC", nativeQuery = true)
    public List<Reward> getAllByCampaignId(Long campaignId);

    @Query(value = "SELECT * FROM reward WHERE reward_type_id = :rewardTypeId AND campaign_id = :campaignId AND deleted_at IS NULL", nativeQuery = true)
    public Reward getByRewardTypeAndCampaignId(Long rewardTypeId, Long campaignId);
}
