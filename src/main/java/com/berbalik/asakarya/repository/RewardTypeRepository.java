package com.berbalik.asakarya.repository;

import com.berbalik.asakarya.entity.RewardType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RewardTypeRepository extends JpaRepository<RewardType, Long> {
}
