package com.berbalik.asakarya.repository.oauth;

import com.berbalik.asakarya.entity.oauth.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findOneByClientId(String clientId);
}
