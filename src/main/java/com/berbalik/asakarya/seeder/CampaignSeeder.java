package com.berbalik.asakarya.seeder;

import com.berbalik.asakarya.entity.*;
import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.*;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.util.DigitGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class CampaignSeeder implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private RewardTypeRepository rewardTypeRepository;

    @Autowired
    private RewardRepository rewardRepository;

    @Autowired
    private CreatorRepository creatorRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    private FaqRepository faqRepository;

    @Autowired
    private DisbursementRepository disbursementRepository;

    @Autowired
    private BookmarkRepository bookmarkRepository;

    @Autowired
    private GalleryRepository galleryRepository;

    private DataInitial dataInitial = new DataInitial();
    private List<Category> categoryList;
    private List<RewardType> rewardTypeList;
    private List<Campaign> campaignList;
    private List<Campaign> activeCampaignList;

    private User admin;
    private User investor;
    private Creator creator;
    private Address investorAddress;

    @Override
    public void run(String... args) throws Exception {
        setAdmin();
        setInvestor();
        setCreator();
        setInvestorAddress();

        log.info("add new category");
        createCategories();
        setCategoryList();

        log.info("add reward type");
        createRewardTypes();
        setRewardTypeList();

        log.info("add campaign");
        createCampaign();
        setCampaignList();

        log.info("add donation");
        createDonations();

        log.info("add history");
        createHistory();

        log.info("add faq");
        createFaq();

        log.info("add disbursement");
        setActiveCampaignList(); // active campaign
        createDisbursement();

        log.info("add bookmark");
        createBookmark();

        log.info("add gallery");
        createGallery();
    }

    public void setAdmin() {
        this.admin = userRepository.getById(1L);
    }

    public void setInvestor() {
        this.investor = userRepository.getById(2L);
    }

    public void setCreator() {
        this.creator = creatorRepository.getByUserId(4L);
    }

    private void createCategories() {
        for (String category: dataInitial.getCategories()) {
            categoryRepository.save(new Category(category));
        }
    }

    public void setCategoryList() {
        this.categoryList = categoryRepository.findAll();
    }

    private void createRewardTypes() {
        for (String rewardType: dataInitial.getRewardTypes()) {
            String[] str = rewardType.split(":");
            Character type = str[0].charAt(0);
            float gte = Float.parseFloat(str[1]);
            float lte = Float.parseFloat(str[1]);
            RewardType rewardType1 = new RewardType(type, gte, lte);
            rewardTypeRepository.save(rewardType1);
        }
    }

    private void setRewardTypeList() {
        this.rewardTypeList = rewardTypeRepository.findAll();
    }

    private void createCampaign() {
        for (String campaignItem: dataInitial.getCampaigns()) {
            String[] str = campaignItem.split("//");
            Integer[] date = Arrays.stream(str[3].split("-"))
                    .mapToInt(Integer::parseInt)
                    .boxed()
                    .toArray(Integer[]::new);
            int categoryIndex = Integer.parseInt(str[0]) - 1;

            Campaign campaign = new Campaign();
            campaign.setTitle(str[1]);
            campaign.setDescription(str[7]);
            campaign.setFundAmount(Float.parseFloat(str[2]));
            campaign.setDue(LocalDate.of(date[0], date[1], date[2]));
            campaign.setLocation(str[4]);
            if (campaign.getTitle().equals("Memotret Objek Langit")) {
                campaign.setStatus(0);
            } else if (campaign.getTitle().equals("Pemotretan Di Garis Perbatasan")) {
                campaign.setStatus(2);
            } else {
                campaign.setStatus(1);
            }
            campaign.setImgUrl(dataInitial.getImgbbUrl() + str[5]);
            campaign.setCreator(creator);
            campaign.setCategory(categoryList.get(categoryIndex));
            Campaign savedCampaign = campaignRepository.save(campaign);

            // create reward for campaign
            String[] rewards = str[6].split(":");
            log.info("create reward for campaign_id {}", savedCampaign.getId());
            createRewards(rewards, savedCampaign);
        }
    }

    public void setCampaignList() {
        this.campaignList = campaignRepository.getList();
    }

    public void setActiveCampaignList() {
        this.activeCampaignList = campaignRepository.findAll();
    }

    private void createRewards(String[] rewards, Campaign campaign) {
        for (String item: rewards) {
            String[] items = item.split("#");
            Reward reward = new Reward();
            reward.setItem(items[1]);
            reward.setImgUrl(dataInitial.getRewardImage());
            reward.setRewardType(rewardTypeRepository.getById(Long.parseLong(items[0])));
            reward.setCampaign(campaign);
            rewardRepository.save(reward);
        }
    }

    public void setInvestorAddress() {
        this.investorAddress = addressRepository.getById(2L);
    }

    private void createDonations() {
        for (Campaign campaign: campaignList) {
            if (campaign.getStatus() == 1) {
                int countDonation = DigitGenerator.getRandomNumberUsingNextInt(3, 30);
                for (int i = 0; i <= countDonation; i++) {
                    float amount = DigitGenerator.getRandomNumberUsingNextInt(10000,600000);
                    Campaign campaignExist = campaignRepository.getById(campaign.getId());

                    Donation donation = new Donation();
                    donation.setNotes(dataInitial.getDonationNotes());
                    donation.setPaymentSlipUrl(dataInitial.getPaymentSlipImage());
                    if (i == countDonation) {
                        donation.setStatus(0);
                    } else if (i == countDonation - 1) {
                        donation.setStatus(2);
                    } else {
                        donation.setStatus(1);
                        campaignExist.setSumDonation(campaignExist.getSumDonation() + amount);
                        campaignExist.setCountDonation(campaignExist.getCountDonation() + 1);
                    }
                    donation.setAmount(amount);
                    donation.setCampaign(campaignExist);

                    if ((i<5) || (i%2==0)) {
                        donation.setAddress(investorAddress);
                        if (amount >= 500000f) {
                            donation.setReward(rewardRepository.getByRewardTypeAndCampaignId(3L, campaignExist.getId()));
                        } else if (amount >= 250000f) {
                            donation.setReward(rewardRepository.getByRewardTypeAndCampaignId(2L, campaignExist.getId()));
                        } else if (amount >= 100000f) {
                            donation.setReward(rewardRepository.getByRewardTypeAndCampaignId(1L, campaignExist.getId()));
                        } else {
                            log.info("{}: Donation without reward", amount);
                        }
                    }
                    donationRepository.save(donation);
                    campaignRepository.save(campaignExist);
                }
            }
        }
    }

    private void createHistory() {
        for (Campaign campaign: campaignList) {
            if (campaign.getStatus() == 1) {
                Campaign campaignExist = campaignRepository.getById(campaign.getId());
                for (String item : dataInitial.getHistories()) {
                    String[] str = item.split("//");

                    History history = new History();
                    history.setTitle(str[0]);
                    history.setActivity(str[1]);
                    history.setImgUrl(dataInitial.getHistoryImage());
                    history.setCampaign(campaignExist);

                    historyRepository.save(history);
                }
            }
        }
    }

    private void createFaq() {
        for (Campaign campaign: campaignList) {
            Campaign campaignExist = campaignRepository.getById(campaign.getId());
            for (String item: dataInitial.getFaqs()) {
                String[] str = item.split("//");

                Faq faq = new Faq();
                faq.setQuestion(str[0]);
                faq.setAnswer(str[1]);
                faq.setCampaign(campaignExist);

                faqRepository.save(faq);
            }
        }
    }

    private void createDisbursement() {
        for (Campaign campaign: activeCampaignList) {
            int idx = 1;
            for (String item: dataInitial.getDisbursements()) {
                Campaign campaignExist = campaignRepository.getById(campaign.getId());
                float fee = (float)(campaignExist.getSumDonation() * 0.05);
                float fundAmount = campaignExist.getSumDonation() - fee;
                float remainingFunds = fundAmount - campaignExist.getSumDisbursement();

                String[] str = item.split("//");
                float amount = Float.parseFloat(str[2]);
                if (amount <= remainingFunds){
                    Disbursement disbursement = new Disbursement();
                    disbursement.setTitle("Pencairan dana ke "+idx);
                    disbursement.setDescription(str[1]);
                    disbursement.setAmount(amount);
                    disbursement.setCampaign(campaignExist);
                    disbursement.setPaymentSlipUrl(dataInitial.getPaymentSlipImage());
                    disbursement.setUser(admin);

                    disbursementRepository.save(disbursement);

                    campaignExist.setSumDisbursement(campaignExist.getSumDisbursement() + amount);
                    campaignRepository.save(campaignExist);
                    idx = idx + 1;
                }
            }
        }
    }

    private void createBookmark() {
        for (Campaign campaign: activeCampaignList) {
            if (campaign.getSumDonation() < campaign.getFundAmount()) {
                Campaign campaign1 = campaignRepository.getById(campaign.getId());
                Bookmark bookmark = new Bookmark();
                bookmark.setUser(investor);
                bookmark.setCampaign(campaign1);

                log.info("campaign {} user {}", campaign1.getId(), investor.getId());
                bookmarkRepository.save(bookmark);
            }
        }
    }

    private void createGallery() {
        int countCategory = 0;
        Long categoryId = 1L;
        for (int i = 1; i <= 30; i++) {
            Gallery gallery = new Gallery();

            gallery.setTitle("Gallery karya ke-" + i);
            gallery.setDescription(dataInitial.getGalleryDescription());
            gallery.setWebsite(dataInitial.getWebsite());
            gallery.setImgUrl1("https://i.ibb.co/N3JCvjk/Bauhaus-Design-Posters.png");
            gallery.setImgUrl2("https://i.ibb.co/rbQ56c2/pameran-karya-foto.png");
            gallery.setImgUrl3("https://i.ibb.co/tzg8QFt/pemotretan-di-garis-perbatasan.png");
            gallery.setCreator(creator);
            countCategory = countCategory + 1;
            gallery.setCategory(categoryRepository.getById(categoryId));
            if (countCategory == 5) {
                countCategory = 0;
                categoryId = categoryId + 1;
                log.info("countCategory {} categoryId {}", countCategory, categoryId);
            }

            galleryRepository.save(gallery);
        }
    }
}
