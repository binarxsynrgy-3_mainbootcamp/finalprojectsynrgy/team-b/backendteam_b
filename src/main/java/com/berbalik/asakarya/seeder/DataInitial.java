package com.berbalik.asakarya.seeder;

import lombok.Data;

@Data
public class DataInitial {
    private String[] categories = new String[]{
            "Desain", "Foto dan Video", "Penerbitan", "Software dan Game", "Musik", "Lainnya"
    };

    private String[] rewardTypes = new String[]{
            // pattern: type:gte:lte
            "A:100000:249999",
            "B:250000:499999",
            "C:500000:9999999999999"
    };

    private String website = "asakarya-dev.herokuapp.com";
    private String imgbbUrl = "https://i.ibb.co/";
    private String rewardImage = "https://i.ibb.co/m9vqjfb/reward.png";
    private String paymentSlipImage = "https://i.ibb.co/5kzwLc4/payment-slip.jpg";
    private String historyImage = "https://i.ibb.co/27Hy8WS/history.jpg";

    private String[] campaigns = new String[]{
            // pattern: categoryId//title//fundAmount//due//location//imgUrl//rewards: typeId#item//description
            "1//Gopher Christmas!//5000000//2021-12-28//Surabaya, Indonesia//DGCq6K1/Gopher-Christmas.png//" +
                    "1#Two e-cards of Gopher:2#Picture e-book:3#Coloring e-book//" +
                    "Saya telah menghabiskan waktu luang saya menggambar hal-hal lucu yang berkaitan dengan pemrograman. Natal akan datang, dan itulah mengapa saya punya ide untuk menggambarkan e-card, e-book aktivitas, e-book mewarnai untuk mereka yang menyukai pemrograman, dan maskot GoLang. Dengan subjek Natal, Anda akan memiliki e-card yang indah tentang kegiatan keluarga Gopher untuk dikirim ke keluarga, teman, dan sebagainya.",
            "1//Nikola Tesla Poster//8000000//2022-04-12//Semarang, Indonesia//L16X9MB/Nikola-Tesla-Poster.png//" +
                    "1#Digital Edition:2#Printed Edition:3#Digitial + Printed Edition//" +
                    "Nikola Tesla adalah salah satu penemu terbesar dalam sejarah. Jika kita sekarang dapat menikmati keajaiban teknologi listrik, itu sebagian besar karena penemuan dan ide-idenya. Bagi saya, Nikola Tesla selalu menjadi sosok yang menarik, seorang jenius yang baru dikenang dalam beberapa tahun terakhir. Beberapa tahun yang lalu saya merancang dan memprogram aplikasi iPad tentang kehidupan Nikola Tesla. Sayangnya, biaya pemeliharaan dan adaptasi aplikasi itu ke versi iOS yang berurutan membuat saya harus meninggalkan proyek tersebut. Namun, saya bertanya-tanya bagaimana saya bisa menunjukkan kehidupan dan penemuan Nikola Tesla dengan cara yang orisinal, tanpa harus menggunakan proyek yang rumit.Beberapa bulan yang lalu saya menemukan solusinya: poster infografis.\n" +
                    "\n" +
                    "Tapi saya tidak ingin itu menjadi poster sederhana, jadi saya menambahkan elemen interaktif: suara. Melalui kode QR kecil yang tercetak di poster (dapat digunakan dengan ponsel apa pun saat ini atau sejenisnya), Anda dapat mengakses delapan file suara (MP3, total durasi audio sekitar setengah jam) yang telah direproduksi, sebagai rekreasi dari suara Nikola Tesla bersama dengan suara lingkungan, beberapa bagian dari karyanya tahun 1919 yang berjudul \"\"My Inventions\"\".",
            "1//TruckTruck: Redesain Truck Bekas untuk Edukasi Keliling//5000000//2021-12-30//Bogor, Indonesia//sPwt0bY/truck-truck.png//" +
                    "1#Thanks e-card:2#Picture e-book:3#Coloring e-book//" +
                    "Kami ingin mengubah truk makanan rusak ini menjadi toko pop-up yang dapat dibagikan dan bengkel desain seluler untuk menetaskan bisnis baru dan menciptakan peluang belajar di masyarakat. Program impian kami untuk TruckTruck adalah membuat dan menyelenggarakan lokakarya yang mengajarkan keterampilan kerajinan dan pembuat. Sesuatu yang kuat terjadi pada seseorang ketika mereka menyadari kemampuan mereka sendiri untuk secara fisik membuat sesuatu di dunia ini.\n" +
                    "\n" +
                    "Sebagai pendiri studio arsitektur desain-make, kami memiliki toko fabrikasi lengkap untuk membuat furnitur dan instalasi khusus untuk proyek kami. Keingintahuan tentang bagaimana kami menggunakan toko kami dan bagaimana memperkenalkan pembuatan ke dalam bidang arsitektur semakin berkembang, setelah menerima minat dari siswa sekolah menengah, sekolah menengah, dan perguruan tinggi yang ingin mempelajari lebih lanjut.\n" +
                    "\n" +
                    "Sayangnya toko kami tidak cukup besar untuk menerima semua orang ke dalamnya, dan jadwal proyek kami membuat sumber daya kami terikat. Jadi, alih-alih menduplikasi toko kami, kami ingin membuat versi seluler yang dapat menjangkau lebih banyak orang untuk memberikan pengenalan keterampilan pembuat yang mungkin menginspirasi menjadi seniman, perancang, tukang kayu masa depan, daftarnya terus berlanjut!",
            "1//Bauhaus Design Posters//5000000//2021-12-29//Medan, Indonesia//N3JCvjk/Bauhaus-Design-Posters.png//" +
                    "1#Digital Edition:2#Printed Edition:3#Digitial + Printed Edition//" +
                    "Terinspirasi oleh desain Abad Pertengahan dan gerakan Bauhaus, saya telah merancang 12 poster yang saya harap Anda akan menyukainya. Saat ini saya sedang mempelajari kursus sablon dan saya berencana untuk mendirikan studio sablon saya sendiri pada tahun 2022 membuat desain serupa dengan desain digital yang dibuat di Illustrator ini. Untuk membantu mendanai peralatan baru dan perlengkapan sablon, saya meluncurkan proyek ini dengan 12 opsi poster dan berbagai hadiah.",
            "2//Film Pendek: Tilik//45000000//2022-08-10//Semarang, Indonesia//ngpRVvp/tilik.jpg//" +
                    "1#Topi:2#Kaos:3#Topi + Kaos//" +
                    "Tilik adalah salah satu film cerdas yang mengangkat pola keseharian masyarakat kita pada umumnya. Film ini menampilkan sebuah kenyataan secara gamblang, tegas dan tepat. Memotret kebiasaan yang sering dihadapi oleh masyarakat dalam kesehariannya membuat film ini rasanya begitu dekat. Hal ini juga berkat local value yang ditampilkan melalui dialek dan bahasa Jawa sepanjang film, seperti mendengar pembicaraan ibu-ibu tetangga dekat rumah. Jika diperhatikan dengan seksama sebenarnya film ini memiliki misi yang besar untuk menjelaskan dampak hoax atau berita bohong yang tersebar luas di lingkungan pergaulan seseorang. Akan selalu ada satu sosok yang dianggap memiliki kemampuan mengakses informasi lebih sehingga apapun yang dikatakannya menjadi benar. Istilah “orang pintar” kemudian dianggap sebagai Tuhan yang mengetahui banyak hal. Terlepas dari informasinya benar atau salah, toh tidak ada yang merasa perlu untuk memverifikasinya. Nyatanya, Indonesia negara yang memiliki budaya kolektif yang besar seolah memberi ruang bagi orang-orang seperti ini.\n" +
                    "\n" +
                    "Tokoh-tokoh utama yang hadir dalam film ini juga merepresentasikan kita secara pribadi. Bu Tejo yang melek teknologi lalu memposisikan diri sebagai yang paling benar. Yu Ning si pembela sejati yang merasa bahwa apa yang dia yakini adalah benar, bahwa Dian bukan perempuan yang aneh-aneh seperti yang diceritakan oleh Bu Tejo walaupun pada kenyataannya Yu Ning tidak melakukan cross check informasi dan hanya menempatkan referensinya sendiri sebagai yang paling benar. ",
            "2//Film Pendek: Anak Lanang//40000000//2022-07-27//Yogyakarta, Indonesia//YZGhkJR/anak-lanang.jpg//" +
                    "1#Topi:2#Kaos:3#Topi + Kaos//" +
                    "Film pendek berjudul ‘Anak Lanang’  ini bercerita tentang empat orang anak SD yang baru saja pulang sekolah dan menuju rumah menaiki becak langganan.Anak-anak tersebut bernama Sul, Sigit, Yudho dan satu lagi yang tidak diketahui namanya. Karakter mereka yang berbeda – beda yang menjadi keunikan mereka masing – masing seperti  Sigit anak bertipikal rajin dan kalem, Yudho dan anak satunya yang sering bertengkar sepanjang perjalanan dan harus dipisahkan oleh Tukang becak. Selain itu, tokoh tukang becak juga mempunyai pandangan bahwa Orang Amerika pintar-pintar. Sepanjang perjalanan pulang di atas becak, mereka membahas berbagai hal seperti tentang anak baru di sekolah mereka, PR, rencana bermain PS, hingga Hari Ibu. Selama diperjalanan ,Yudho dan anak yang diketahui namanya sempat saling ejek dengan menggunakan sosok ibu sebagai bahan ejekannya.\n" +
                    "\n" +
                    "Film pendek ini mengambil latar di perkampungan di Jogja saat pulang sekolah. Pada hari itu juga bertepatan dengan Hari Ibu. Bahasa yang digunakan yaitu Bahasa Jawa seperti kehidupan sehari-hari masyarakat Jawa pada umumnya khususnya masyarakat Jogja tetapi penonton yang tidak bisa berbahasa Jawa tetap bisa menikmati dengan malalui subtitle. Proses pengerjaan film pendek menggunakan pengambilan gambar dengan teknik one take shot. Teknik one take shot merupakan teknik yang salah satu atau adegan filmnya menggunakan satu kali take atau pengambilan gambar yang menjadikan filmnya tanpa jeda dari awal hingga akhir cerita atau adegan.",
            "2//Pameran Karya Foto: Dari Sisi Jakarta//20000000//2021-12-30//Jakarta, Indonesia//rbQ56c2/pameran-karya-foto.png//1#Topi:2#Kaos:3#Topi + Kaos//" +
                    "Hi, hallo kawan-kawan!\n" +
                    "\n" +
                    "Saya Anest, saya adalah seorang freelance photographer dan social media savvy based di Jakarta, selama 3 tahun ke belakang saya menggeluti photography sambil bekerja di beberapa digital agency di Jakarta sebagai KOL/Influencer Manager. Pernah dipercaya mengerjakan beberapa project photoshoot untuk social media content brand, digital artwork untuk musisi, dan behind the scenes shooting webseries atau iklan. \n" +
                    "\n" +
                    "Disisi lain, saya juga adalah seorang Street Photograper yang senang mengabadikan memori. Di tahun 2020-2021 ini, saya aktif memotret kondisi Jakarta dan sekitarnya untuk menggambarkan dan menangkap momen yang terjadi selama pandemi. Tahun depan rencananya saya ingin membuka sebuah pameran fotografi bertemakan \"\"Dari Sisi Jakarta\"\" untuk mengenang bahwa kita pernah melewati masa sulit. \n" +
                    "\n" +
                    "\n" +
                    "Dukungan dan apresiasi dari kawan-kawan di sini sangat berarti buat saya dan bisa jadi bahan bakar saya untuk terus berkarya dan berbagi.\n" +
                    "\n" +
                    "\n" +
                    "Terima kasih, \n" +
                    "\n" +
                    "Salam semangat!",
            "2//Pemotretan Di Garis Perbatasan//24000000//2022-04-16//Bandung, Indonesia//tzg8QFt/pemotretan-di-garis-perbatasan.png//" +
                    "3#Printed and signed photo book//" +
                    "Hai! Saya Fabio, saya 25 dan saya Bandung. Passion saya adalah fotografi, sejak saya memiliki kamera tua ketika masih kecil dan mengambil gambar di sana-sini di seluruh rumah! Pengalaman hidup saya telah membawa saya untuk menyentuh perbedaan sosial dan ekonomi yang dalam beberapa dekade terakhir semakin nyata dan luar biasa. Saya ingin membuat pemotretan di mana saya membawa celah ini menjadi terang, memotret orang dan momen kehidupan sehari-hari yang mirip dan sangat berbeda untuk mata sosial. Semua termasuk dalam serangkaian bidikan yang siap membuat Anda berpikir. Biaya proyek, meskipun rendah, saat ini melebihi kemungkinan saya. Uang yang diminta akan digunakan untuk perjalanan, perjalanan dan pembelian lensa baru untuk menggantikan yang lama. Aku percaya padamu. Terima kasih. Fabio.",
            "2//Memotret Objek Langit//2000000//2021-12-30//Jember, Indonesia//LSN0gLn/memotret-objek-langit.jpg//" +
                    "1#5 Gambar HD (soft-file):2#10 Gambar HD (soft-file):3#10 Gambar HD (soft-file) dan 2 Gambar cetak//" +
                    "Halo semuanya. Hanya ingin berterima kasih kepada siapa pun yang memberikan waktu untuk mendengarkan saya, saya hanya seorang pria yang telah mencitrakan bintang & planet selama 2 tahun terakhir dan saya memanfaatkan alat dan peralatan yang saya miliki untuk mengambil gambar sebaik mungkin. 2 juta hanyalah sebagian kecil dari apa yang benar-benar saya butuhkan, tetapi sampai saat ini tidak mungkin bagi saya untuk mendanai semua ini sendiri, dan saya juga merasa tidak nyaman meminta publik untuk sepenuhnya mendanai ini, jadi saya beralih ke internet untuk mendorong saya ke arah yang benar, untuk mendorong saya bekerja menuju tujuan yang lengkap..\n" +
                    "\n" +
                    "Tentu saja, seperti yang akan dilakukan semua orang, saya ingin dapat meningkatkan beberapa peralatan saya, ada banyak hal yang dapat membuka pintu baru bagi saya, sesuatu yang revolusioner, dan beberapa minggu teknologi kecil. Hanya beberapa di antaranya yang mencakup hal-hal seperti Kamera Astronomi CCD, Filter Surya, Filter H-Band, Dudukan Teleskop EQ3 (Sebuah dudukan yang melacak langit, memungkinkan gambar eksposur lama) dan alat fotografi umum lainnya.\n" +
                    "\n" +
                    "Saya sangat berharap proyek saya menarik minat publik, seperti yang saya katakan dalam skema hadiah, saya akan mengirimkan gambar saya ke pemberi donasi yang membantu saya mencapai tujuan saya. Pada dasarnya apa pun yang dapat saya lakukan untuk membuat ini berharga bagi Anda, saya sangat berharap untuk mendengar dari beberapa dari kalian segera.",
            "3//Africa on Safari - The Book//18000000//2022-03-13//Jakarta, Indonesia//Zd3Nn6s/Africa-on-Safari.png//" +
                    "1#10 postcard Africa on Safari:2#Book Digital Edition:3#Book Printed Edition//" +
                    "Selama beberapa tahun terakhir, kami telah memotret satwa liar terbaik Afrika menggunakan serangkaian teknik fotografi inovatif termasuk drone yang dikendalikan dari jarak jauh, kamera tersembunyi, dan quadcopters. Di Africa on Safari, kami menampilkan lebih dari 140 gambar kami yang paling menarik di sepanjang dengan cerita di balik setiap foto. Di bagian Behind the Lens dari buku ini, kami memberikan saran yang sangat berharga tentang cara mendapatkan hasil maksimal dari safari Afrika; kiat tentang area terbaik untuk dikunjungi, cara memilih pemandu hebat, apa yang akan Anda lakukan perlu dalam kit medis Anda, kamp terbaik, kapan harus bepergian, bagaimana cara berkeliling dan banyak lagi.\n" +
                    "\n" +
                    "Fotografi bukanlah sumber pendapatan utama kami, jadi kami dengan senang hati mengungkapkan semua rahasia fotografi kami yang memungkinkan Anda mendapatkan gambar yang lebih baik saat Anda memulai safari. Proyek ini adalah karya cinta yang telah melihat kami melintasi Kenya, Afrika Selatan, Tanzania, Botswana, Zimbabwe, dan Rwanda untuk mencari satwa liar paling menakjubkan di Afrika dan sekarang Anda dapat memiliki salinan format besar ini (32cm x 26cm), gambar mengkilap buku. Kompendium mewah setebal 204 halaman ini akan memberikan gambaran menakjubkan tentang Afrika, melestarikan keindahannya yang liar dan emosinya yang mentah selamanya. Dari kebrutalan rantai makanan hingga foto-foto lucu hewan tanpa hambatan. Dari matahari terbit dan terbenam yang megah hingga lanskap yang merenung, tidak ada teater yang lebih hebat dari alam itu sendiri.\n" +
                    "\n" +
                    "Kami tahu Anda akan menyukai Afrika di Safari, dan begitu juga dengan orang-orang yang Anda pilih untuk memberikan buku tersebut. Dengan mendukung proyek ini, Anda tidak hanya akan menerima buku Anda sebelum diluncurkan, Anda juga akan menikmati laporan kemajuan reguler dan video di balik layar.",
            "3//Catatan Najwa//15000000//2022-03-19//Bandung, Indonesia//mqVq6NZ/catatan-najwa.jpg//" +
                    "1#Buku edisi digital:2#Buku edisi cetak:3#Kaos + Buku edisi cetak//" +
                    "Buku ini berisi tentang sinopsis singkat acara talkshow dan refleksi penulis saat memandu program talkshow di salah satu stasiun TV swasta. Acara yang mengupas tentang peristiwa-peristiwa politik yang sedang terjadi serta permasalahan bangsa yang layak untuk disoroti seperti perebutan jabatan dan kekuasaan, tindak korupsi, dan pelanggaran aturan-aturan. \n" +
                    "\n" +
                    "Buku ini juga memuat sekelumit gambaran karakter tokoh-tokoh politik dan pengalaman mereka dalam melakukan perubahan baik dan kontribusi mereka bagi kemajuan negeri. Pengalaman para tokoh negeri ini ditulis dengan kalimat yang penuh makna, mengena dan sangat menginspirasi serta mengajak pembaca untuk semakin peduli dan tidak berdiam diri. \n" +
                    "\n" +
                    "Buku ini juga mengkritisi berbagai peristiwa politik yang sedang hangat terjadi agar pembaca semakin mengerti dan mencintai bangsa ini. Penggunaan kalimat yang padat berisi dengan rima yang khas pada setiap catatan penulis sangat mengkritik dan menyindir kondisi politik serta birokrasi masa kini. Sindiran dan kritikan yang dibalut dalam sebuah tulisan ini mengajak pembaca untuk merenungkan kembali aksi nyata yang sudah dilakukan bagi bangsa ini.",
            "3//Sensitive Novel - Paranormal Thriller//5000000//2022-02-07//Surabaya, Indonesia//MVyrTGZ/sensitive-novel.png//" +
                    "1#Buku edisi digital:2#Buku edisi cetak:3#Kaos + Buku edisi cetak//" +
                    "Saya telah mengerjakan novel ini selama lebih dari sepuluh tahun, terinspirasi oleh hasrat saya terhadap paranormal dan rumah masa kanak-kanak saya. Cerita telah berkembang selama bertahun-tahun saat saya dewasa, dan setelah mendapatkan perspektif baru saat mengunjungi bulan madu kami di tahun 2020, saya bangga dengan cerita dan tempat yang harus saya bagikan. Ini adalah ucapan terima kasih serta dedikasi untuk adik perempuan malaikat saya. Ini adalah kisah pahit persahabatan, menemukan diri sendiri, dan mengatasi kehilangan dan perubahan. Saya harap ini akan menjadi film thriller mencekam yang sulit untuk dihentikan, juga.\n" +
                    "\n" +
                    "Saya selalu ingin menulis; Saya telah menulis buku untuk bersenang-senang sejak saya berusia sekitar lima tahun. Sekarang, saya ingin menjadikannya karier. Sayangnya, tujuan ini tidak mudah, karir juga tidak stabil atau terjamin. Tapi itu gairah saya, dan saya butuh sedikit bantuan.\n" +
                    "\n" +
                    "Saya berencana menyelesaikan buku ini saat cuti hamil sebelum putri saya lahir, tetapi akhirnya melahirkan prematur 2 bulan melalui operasi caesar darurat. Itu mengirim waktu saya untuk menulis dan mempersiapkan diri untuk menjadi ibu yang terburu-buru. Untungnya, saya mendapatkan bayi perempuan seberat 2 pon yang sangat kuat dan sehat sebagai balasannya. Dia adalah inspirasi terbesar saya.\n" +
                    "\n" +
                    "Saya sekarang menjadi ibu rumah tangga sementara, dan kami berjuang untuk memenuhi kebutuhan dengan satu penghasilan. Saya berencana untuk menyelesaikan buku dan menerbitkan sendiri pada April 2022 sementara saya mengirimkan manuskrip ke penerbit. Pendukung proyek saya tidak hanya akan mendukung karier saya dan membantu kami memenuhi kebutuhan saat saya menulis, tetapi juga akan membantu membayar biaya promosi, pencetakan, dan pemesanan salinan buku saya setelah diterbitkan.",
            "4//Hirelang: aplikasi mobile gratis untuk belajar bahasa//21000000//2022-10-12//Makassar, Indonesia//1GHPXzX/Hirelang.png//" +
                    "2#Nama terdaftar di pengakuan aplikasi:3#Kaos + Nama terdaftar di pengakuan aplikasi//" +
                    "Hirelang adalah aplikasi seluler gratis untuk pembelajaran Bahasa Inggris. Aplikasi ni ditujukan untuk orang asing yang mencari pekerjaan di negara baru. Versi dasar aplikasi ini sedang dalam tahap pengembangan dan akan dirilis dalam beberapa minggu ke depan di AppStore dan Google Play. Kursus difokuskan pada cabang yang populer di kalangan pekerja imigran, misalnya, pertanian atau bekerja di lokasi konstruksi.",
            "4//Fuel Bus//30000000//2022-12-21//Surabaya, Indonesia//L68y45H/Fuel-Bus.png//" +
                    "1#1 Free Delivery - 1 Day:2#3 Free Deliveries - 10 Days:3#6 Free Deliveris - 1 Month//" +
                    "Fuel Bus adalah cara paling nyaman untuk mengisi tangki Anda. Apakah Anda sedang bekerja dan membutuhkan bensin di kendaraan Anda, atau di bersiap untuk tidur dan menyadari bahwa Anda tidak ingin bangun pagi untuk mengisi bensin. Kami akan ada untuk mengisi tangki Anda. Kami dapat digunakan dalam berbagai cara. Bukan hanya kendaraan saja yang kami isi. Semuanya mulai dari perahu hingga mesin pemotong rumput. Kami tidak hanya menawarkan jasa untuk membeli bensin, tetapi juga menerima pesanan makanan ringan dari toko serba ada. Cukup pilih produk yang Anda inginkan dan driver akan mengantarkannya kepada Anda.\n" +
                    "\n" +
                    "Apa yang membuat kami berbeda dari pesaing kami adalah kemampuan karyawan kami untuk bekerja Kapan pun mereka mau. Anda dapat membandingkan kami dengan aplikasi seperti Uber. Karyawan kami harus mendapatkan kaleng tertentu yang dapat ditempatkan di mobil mereka untuk diisi dan dikirim ke tujuan yang diinginkan. Ketika maksud saya siapa pun dapat bekerja untuk kami, saya bersungguh-sungguh. Ini adalah proses sederhana untuk mendaftar memasukkan kaleng bensin Anda ke dalam mobil Anda, lalu menyalakan aplikasi untuk melihat siapa yang membutuhkan bensin.",
            "4//Neighborfood//28000000//2022-11-10//Denpasar, Indonesia//nB6wytm/Neighborhood.png//" +
                    "1#Early Access:2#Listing in app acknowledgements//" +
                    "Hai semua, Bosan restoran tutup lebih awal? atau kamu malah bosan makan makanan yang sama terus menerus? Di sinilah aplikasi kita akan muncul. Truk makanan harus menyerahkan menu mereka pada malam sebelumnya sehingga polling harian dapat diatur untuk melihat lingkungan mana yang tertarik dengan truk makanan mana yang datang ke lingkungan mereka. Artinya jika lingkungan Susan memiliki 10 orang yang ingin nasi goreng dan Frank tinggal di lingkungan lain dan hanya dirinya yang tertarik, truk tersebut akan pergi ke lingkungan Susan.\n" +
                    "\n" +
                    "Neighborfood juga akan menawarkan pemesanan online sehingga Anda dapat mengambil makanan Anda daripada harus menunggu dalam antrean. Ini menghilangkan pengambilan makanan biasa yang membosankan, menghilangkan makan yang sama setiap hari karena menu akan berubah setiap hari atau setiap minggu. Betapa senangnya bisa melangkah keluar rumah dan bisa mendapatkan makanan yang berbeda setiap hari.... tanpa harus mengemudi atau berbelanja. Pelanggan juga dapat memilih pilihan makanan yang mereka inginkan tersedia di truk.",
            "4//Roadways Board Game//25000000//2022-09-09//Aceh, Indonesia//VSszJ6S/Boardways.png//" +
                    "3#Roadways Box Set//" +
                    "Permainan Roadways memliki desain yang sangat sederhana, tujuan permainan ini adalah menjadi pemain pertama yang membangun jalan lengkap ke tengah papan. Untuk memulai permainan, kedua pemain menggambar 4 kartu dan menempatkannya menghadap ke atas. di depan mereka Setiap pemain bergantian memainkan kartu di papan dengan tujuan untuk membangun jalan mereka ke tengah atau mengalihkan jalan lawan mereka.\n" +
                    "\n" +
                    "Permainan papan Roadways memiliki berbagai macam strategi permainan yang dapat bervariasi tergantung pada pemain atau ubin di tangan Anda. Beberapa pemain mungkin memilih untuk bertahan lebih awal, menghambat lawan mereka, sementara yang lain mungkin memilih untuk menyebarkan jalan mereka dengan cepat memungkinkan serangan dari berbagai sudut. Game bisa menjadi cara yang menyenangkan untuk menyatukan keluarga dan teman. Jadi, jika Anda mencari game baru untuk dimainkan liburan akhir tahun ini, pertimbangkan untuk mendukung Roadways, hadiah yang sempurna untuk teman dan keluarga.",
            "4//Bot Pendengar Musik//15000000//2022-03-22//Semarang, Indonesia//Xz2zHSM/bot-pendengar-musik.jpg//" +
                    "1#Early Access:2#Listing in app acknowledgements//" +
                    "\"Seperti yang kita ketahui, penggunaan BOT untuk moderasi maupun hiburan sudah semakin banyak digemari. Terlebih penggunaan music dalam bot.\n" +
                    "\n" +
                    "Namun dalam akhir waktu ini, beberapa bot telah menutup servisnya dikarenakan masalah Copyright Infringement dari YouTube. Oleh karena itu kami selaku Tim Nasi Padang memutuskan untuk membuka modul music untuk server MAHA5. Kami juga membuat bot Andini Music untuk memastikan teman-teman bisa mendengarkan music dengan mengikuti aturan dari YouTube. Salah satu poin terpenting adalah membuat bot open-source dan juga memberikan akses bot secara keseluruhan. Artinya tidak ada fitur premium seperti bot pada umumnya.\n" +
                    "\n" +
                    "Namun dengan demikian, kebutuhan server menjadi lebih berat dikarenakan module yang cukup memberatkan. Oleh karena itu kami memutuskan untuk membuka donasi bagi siapapun yang ingin membantu bot kami. Kami hanya bisa menawarkan role karena kami dilarang untuk men-premiumkan bot music. Namun berikut adalah janji kami untuk bot kedepannya.\n" +
                    " \n" +
                    "\n" +
                    "1. Mempertahankan music module untuk 2 bot. MAHA5BOT dan ANDINIMUSIC.\n" +
                    "\n" +
                    "2. Apabila dana mencukupi, kami akan menambah bot music lagi.\n" +
                    "\n" +
                    "3. Memastikan bot bisa beroperasi penuh dengan kualitas audio yang tinggi.\n" +
                    "\n" +
                    "4. Menambah fitur Fun dari MAHA5BOT.\n" +
                    "\n" +
                    "\n" +
                    "Demikian adalah hal yang akan kami tawarkan. Dengan kalian, kita akan bisa mempertahankan dan mengembangkan BOT ini. Demikian informasi ini saya sampaikan.\n" +
                    " \n" +
                    "\n" +
                    "- Osvster dan Tim Nasi Padang\"",
            "5//Seperti Rahim Ibu - Efek Rumah Kaca//20000000//2022-05-11//Palembang, Indonesia//ZxYQ7ML/seperti-rahim-ibu.jpg//" +
                    "1#Notebook:2#DVD Musik:3#Kaos + DVD Musik//" +
                    "“Seperti Rahim Ibu” adalah sebuah perumpamaan, di mana dalam rahim ibu  terdapat sel-sel yang awalnya rapuh menjadi saling bersatu dan saling menguatkan, yang  hanya satu sel menjadi jutaan sel dan hingga pada akhirnya menjadi bentuk manusia. Penggambaran Negeri “Seperti Rahim Ibu” hadir menjadi sebuah gagasan-gagasan baru yang melahirkan optimisme, semangat, harapan jadi satu kesatuan dan suatu keajaiban. Banyak pertanyaan yang terlintas, “kenapa harus rahim Ibu? Apakah seistimewa itu sebuah organ dalam tubuh manusia hingga dijadikan sebuah perumpamaan?”\n" +
                    "\n" +
                    "Secara harfiah, rahim merupakan salah satu organ yang berada dalam kesatuan sistem reproduksi wanita. Organ ini terletak di bagian tengah dari rongga panggul, tepatnya di belakang kandung kemih dan di depan rektum. Fungsi rahim adalah menerima sel telur yang dibuahi, berubah menjadi janin dan akan menahannya selama perkembangan. Betapa mulianya seorang ibu dalam merawat kehidupan, tak peduli bagaimanapun raganya yang rapuh, Ibu, hadir sebagai penenang dari raga-raga yang mungkin akan membahayakan nyawanya sendiri.\n" +
                    "\n" +
                    "Alangkah indahnya negeri ini jika seperti rahim Ibu, dirawat, diberi kehidupan, dikasihi dan dilindungi seperti anak sendiri. Kita hanya bisa berusaha, berusaha untuk selalu menjaga dan merawat apa yang kita miliki sekarang, seperti kemuliaan seorang Ibu yang menjaga kita sejak dalam kandungan.",
            "5//Zona Nyaman - Fourtwnty//19000000//2022-04-30//Semarang, Indonesia//qWy4Q3X/zona-nyaman.png//" +
                    "1#Notebook:2#DVD Musik:3#Kaos + DVD Musik//" +
                    "Lagu ini seolah mengajak kita untuk keluar dari zona nyaman. Khususnya bagi kita yang bekerja di bidang yang sebetulnya tidak sesuai dengan passion yang dimiliki. Keterbatasan lapangan pekerjaan ditambah tuntutan kebutuhan serta dorongan orang tua akhirnya memaksa kita untuk menjalani kehidupan dengan pekerjaan yang sebenarnya tidak kita inginkan.\n" +
                    "\n" +
                    "\"\"Kalau bekerja sekedar bekerja, kera juga bekerja\"\" (Buya Hamka). Ungkapan dari Buya Hamka tersebut mengandung arti yang kurang lebih sama dengan lirik “kita ini insan bukan seekor sapi”. Meskipun kita sanggup menyelesaikan pekerjaan yang ada, hasilnya tetaplah tidak akan semaksimal saat kita mengerjakannya dengan hati (karena sesuai passion). Untuk itulah perlu kita renungkan, apakah selama ini kita sudah bekerja dengan hati apa belum. Kalau belum maka segeralah beranikan diri untuk berubah, sebab bagaimanapun juga bekerja dengan hati dan sesuai dengan passion yang dimiliki akan lebih mendekatkan kita kepada kesuksesan.",
            "5//We Met On The Stairs Of A Bungalow - Spoken Word Album//25000000//2022-01-24//Jakarta, Indonesia//h8jrnBN/we-met-on-the-stairs-of-a-bungalow.png//" +
                    "1#Notebook:2#DVD Musik:3#Kaos + DVD Musik//" +
                    "Hai, saya Rhian seorang penyair dan seniman.\n" +
                    "\n" +
                    "Saya telah membuat kata yang diucapkan untuk album musik yang disebut 'We Met On The Stairs Of A Bungalow'. Album ini menggabungkan puisi kata yang diucapkan dengan musik dan mencakup kolaborasi dengan sesama penyair, artis hiphop, dan musisi.\n" +
                    "\n" +
                    "Impian saya adalah untuk merilis album di vinyl 12\". Saya seorang pecinta vinyl besar tetapi Anda tidak memerlukan pemutar rekaman untuk menikmatinya. LP akan datang dengan kode unduhan digital sehingga Anda dapat mengakses album secara digital serta menerima rekaman nyata yang dapat Anda mainkan atau gunakan sebagai karya seni orisinal yang keren untuk rumah Anda.\n" +
                    "\n" +
                    "Ada juga peluang 'tambahan' bagi Anda untuk mendapatkan tiket ke pesta peluncuran yang akan berlangsung di London pada tahun 2022. Pesta ini akan menjadi tempat yang fantastis untuk berbaur dan berjejaring dengan penyair, musisi, dan kreatif. Beberapa talenta lisan terbaik di Inggris akan tampil pada malam hari dan bahkan ada 5 slot mikrofon terbuka eksklusif yang tersedia jika Anda ingin mendapat kesempatan untuk tampil.\n" +
                    "\n" +
                    "Jumlah target saya murni untuk membayar vinyl pressing dan biaya kickstarter. Semua biaya desain dan produksi telah saya tanggung sendiri dan tidak mencari keuntungan, hanya untuk mewujudkan impian saya agar album saya dicetak di vinyl.\n" +
                    "\n" +
                    "Album itu sendiri telah menjadi karya cinta dan saya telah bekerja sangat keras untuk memproduksinya. Ini termasuk kolaborasi dengan penyair, musisi, artis hiphop, dan produser musik yang luar biasa. Premis album ini adalah perjalanan ke alam bawah sadar saya dan menangani masalah seputar kesehatan mental, hubungan, ketidakadilan sosial, kreativitas, dan pertumbuhan pribadi. Ini melambangkan penutupan dan awal yang baru dan saya sangat bangga karenanya. Saya sangat berharap Anda dapat mendukung saya dan membantu mewujudkan impian saya, tetapi terutama saya harap Anda menikmati album ini dan menyukainya sama seperti saya.",
            "5//Pengembangan AniFunkot Indonesia//5000000//2021-12-29//Yogyakarta, Indonesia//qyKwn4k/pengembangan-Anifunkot-indonesia.jpg//" +
                    "1#Notebook:2#DVD Musik:3#Kaos + DVD Musik//" +
                    "AniFunkot Indonesia adalah circle doujin musik yang memiliki spesialisasi pada genre EDM lokal Indonesia. Tema yang kami usung adalah tema Kejepangan seperti Vocaloid, Anisong, Game Soundtrack, dan sebagainya. Selain itu, kami juga membuat beberapa lagu original yang bisa teman-teman dengarkan di album maupun track terpisah kami.\n" +
                    "\n" +
                    "Didirikan pada tahun 2019, AniFunkot Indonesia telah mengeluarkan album kompilasi pertama yang diberi judul “Heaven FUNKY” pada acara Comic Frontier 14 pada tahun 2020 lalu. Kini AniFunkot Indonesia menaungi beberapa konten kreator untuk membuat hari-harimu lebih menyenangkan dengan musik EDM lokal racikan kami.\n" +
                    "\n" +
                    "Perjalanan AniFunkot Indonesia masih panjang. Mohon dukungan teman-teman agar para kreator musik yang bernaung di bawah kami semakin bersemangat membuat karya serta menghibur teman-teman semua.\n" +
                    "\n" +
                    "Terima kasih.",
            "6//Donat Dino//7000000//2022-01-18//Malang, Indonesia//N6LMYxP/Donat-Dino.png//" +
                    "1#Voucher senilai 50K:2#Voucher senilai 100K:3#Voucher senilai 150K//" +
                    "Halo, saya Emiliano, seorang pengusaha muda dengan ide inovatif, saya ingin berbagi dengan Anda proyek yang sangat menjanjikan yang telah memotivasi saya sejak lama dan akan membuat banyak orang bahagia. Ide luar biasa untuk menyesuaikannya dalam bentuk dinosaurus muncul, sesuatu yang sangat baru bagi publik dan sangat menjanjikan.\n" +
                    "\n" +
                    "Donat Dino adalah donat lezat dalam bentuk banyak dinosaurus, sesuatu yang belum pernah terlihat sebelumnya, yang sangat ideal untuk membuat anak-anak tersenyum dan menyampaikan perasaan bahagia untuk donat, Dengan hasil, itu dimaksudkan untuk membuka restoran donat dino, memperoleh bahan dan real estat untuk mendiversifikasi produk kami sehingga mereka adalah donat paling terkenal di dunia!Hal ini juga dimaksudkan untuk membuat museum kecil di restoran yang sama dengan informasi, patung, fakta menarik tentang dinosaurus untuk membuat kunjungan Anda lebih bermakna.\n" +
                    "\n" +
                    "Saya sangat senang dapat mencapai proyek ini untuk memberi tahu orang-orang bahwa ada lebih banyak cara untuk mencicipi donat lezat yang sekarang berbentuk dinosaurus!",
            "6//Pengembangan Channel Youtube Arsitektur//5000000//2021-12-29//Tanggerang, Indonesia//1v1Wb4z/youtube-arsitektur.jpg//" +
                    "1#Sticker//" +
                    "Channel ini dibuat untuk kalian yang sedang mencari rumah atau tertarik dengan dunia arsitek. Kami menyajikan review - review yang jujur untuk kalian semua tentang segala hal yang berkaitan dengan eksterior, interior, pengerjaan, budget, pemilihan material dan yang lainnya yang berhubungan dengan rumah. Tim kami berisi arsitek - arsitek praktisi yang sudah berpengalaman mendesign, membuat,mengelola workshop dan kantor arsitek lebih dari 15 tahun dan juga orang Media yang berpengalaman selama 19 tahun di dunia Jurnalistik dan Media Digital serta Multimedia.\n" +
                    "\n" +
                    "Untuk keberjalanan channel kami, kami membutuhkan dukungan teman-teman agar kami dapat terus berkarya dan menyajikan beragam video yang dapat kalian nikmati. Jangan lupa lihat profile kami dan untuk melihat channel link video kami di Youtube. Semoga kalian terhibur dan mendapatkan pengalaman di dunia arsitektur.",
            "4//Asakarya - Crowdfunding System//12000000//2021-12-27//Jakarta, Indonesia//rFTjzSf/asakarya.png//" +
                    "1#Notebook:2#Tumblr:3#Kaos//" +
                    "Indonesia memiliki segudang pelaku industri kreatif yang hebat, namun sayangnya banyak dari mereka gagal mengembangkan karyanya karena keterbatasan dana. Melalui AsaKarya, kami hadir sebagai wadah kemajuan Industri Kreatif di Indonesia. AsaKarya merupakan suatu platform socinvest crowdfunding yang berfokus pada Industri Kreatif. Asakarya didirikan pada tahun 2021 di Indonesia. Berisi orang-orang yang berasal dari berbagai latar belakang namun memiliki satu Visi dan Misi yang sama untuk membangun AsaKarya."
    };

    private String donationNotes = "Ide yang sangat menarik! Gasabar nunggu ide ini terealisasi, semangat buat temen-temen pelaku industri kreatif!";

    private String[] histories = new String[]{
            // pattern: title//description
            "Campaign Terverifikasi!//Donasi dapat dilakukan setelah campaign terverifikasi.",
            "Yuk dukung Pelaku Industri Kreatif!//Kita udah mulai segala persiapan untuk memulai proyek ini, kita sangat bersemangat untuk merealisasikan ide ini!.",
            "Terimakasih telah percaya dengan ide kami!//Kita lagi nyiapin reward nih! pastinya buat rekan asa yang telah mendukung proyek ini dengan melakukan donasi sesuai batas donasi yang telah ditentukan.",
    };

    private String[] faqs = new String[] {
            // pattern: question//answer
            "Kapan reward diberikan?//Reward akan segera diberikan ketika target donasi telah mencapai 100%.",
            "Apakah donasi bisa dilakukan lebih dari satu kali?//Tentu! tidak ada batasan berapakali untuk melakukan donasi pada campaign ini..",
            "Saya tertarik untuk bekerja sama dengan proyek ini, bagaimana saya bisa menghubungi kreator?//Dengan senang hati! silahkan kirim email ke creator@mail.com.",
            "Jika target donasi tidak terpenuhi, apakah reward gagal diberikan?//Reward hanya dapat diberikan jika target donasi telah mencapai 100%. Kami sangat berharap ide ini dapat terealisasi, yuk ajak teman dan keluarga untuk berpartisipasi!"
    };

    private String[] disbursements = new String[] {
            // pattern: title//description//amount
            "Pencairan dana pertama//Transfer ke rekening BCA a/n Budi Doremi//1000000",
            "Pencairan dana kedua//Transfer ke rekening BCA a/n Budi Doremi//1500000",
            "Pencairan dana ketiga//Transfer ke rekening BCA a/n Budi Doremi//1000000",
            "Pencairan dana keempat//Transfer ke rekening BCA a/n Budi Doremi//500000",
            "Pencairan dana kelima//Transfer ke rekening BCA a/n Budi Doremi//2000000",
            "Pencairan dana keenam//Transfer ke rekening BCA a/n Budi Doremi//1000000"
    };

    private String galleryDescription = "Saya Anest, saya adalah seorang freelance photographer dan social media savvy based di Jakarta, " +
            "selama 3 tahun ke belakang saya menggeluti photography sambil bekerja di beberapa digital agency di Jakarta sebagai KOL/Influencer Manager. " +
            "Pernah dipercaya mengerjakan beberapa project photoshoot untuk social media content brand, digital artwork untuk musisi, " +
            "dan behind the scenes shooting webseries atau iklan.";
}
