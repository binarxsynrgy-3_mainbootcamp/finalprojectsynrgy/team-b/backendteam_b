package com.berbalik.asakarya.seeder;

import com.berbalik.asakarya.entity.Address;
import com.berbalik.asakarya.entity.Creator;
import com.berbalik.asakarya.entity.Profile;
import com.berbalik.asakarya.entity.oauth.Client;
import com.berbalik.asakarya.entity.oauth.Role;
import com.berbalik.asakarya.entity.oauth.RolePath;
import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.AddressRepository;
import com.berbalik.asakarya.repository.CreatorRepository;
import com.berbalik.asakarya.repository.ProfileRepository;
import com.berbalik.asakarya.repository.oauth.ClientRepository;
import com.berbalik.asakarya.repository.oauth.RolePathRepository;
import com.berbalik.asakarya.repository.oauth.RoleRepository;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
@Service
@Slf4j
public class OauthSeeder implements ApplicationRunner {

    private static final String TAG = "DatabaseSeeder {}";

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private CreatorRepository creatorRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private RolePathRepository rolePathRepository;

    private String defaultPassword = "password";

    private DataInitial dataInitial = new DataInitial();

    private String[] users = new String[]{
            "admin@mail.com:Nicholas Saputra:ROLE_INVESTOR ROLE_CREATOR ROLE_ADMIN ROLE_SUPERUSER:sKvX5yD/admin.jpg",
            "investor@mail.com:Raditya Dika:ROLE_INVESTOR:QDDp1wh/investor.jpg",
            "anonymous@mail.com:You Know Who I Am:ROLE_INVESTOR:HhVDr9v/anonymous.jpg",
            "creator@mail.com:Reza Rahadian:ROLE_INVESTOR ROLE_CREATOR:JQH0Gv3/creator.jpg"
    };

    private String[] clients = new String[]{
            "asakarya-app:ROLE_READ ROLE_WRITE",
            "asakarya-web:ROLE_READ ROLE_WRITE"
    };

    private String[] roles = new String[] {
            "ROLE_SUPERUSER:user_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_ADMIN:user_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_INVESTOR:user_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_CREATOR:user_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_READ:oauth_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS",
            "ROLE_WRITE:oauth_role:^/.*:GET|PUT|POST|PATCH|DELETE|OPTIONS"
    };

    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        String password = encoder.encode(defaultPassword);

        this.insertRoles();
        this.insertClients(password);
        this.insertUser(password);
    }

    @Transactional
    private void insertRoles() {
        for (String role: roles) {
            String[] str = role.split(":");
            String name = str[0];
            String type = str[1];
            String pattern = str[2];
            String[] methods = str[3].split("\\|");
            Role oldRole = roleRepository.findOneByName(name);
            if (null == oldRole) {
                oldRole = new Role();
                oldRole.setName(name);
                oldRole.setType(type);
                oldRole.setRolePaths(new ArrayList<>());
                for (String m: methods) {
                    String rolePathName = name.toLowerCase()+"_"+m.toLowerCase();
                    RolePath rolePath = rolePathRepository.findOneByName(rolePathName);
                    if (null == rolePath) {
                        rolePath = new RolePath();
                        rolePath.setName(rolePathName);
                        rolePath.setMethod(m.toUpperCase());
                        rolePath.setPattern(pattern);
                        rolePath.setRole(oldRole);
                        rolePathRepository.save(rolePath);
                        oldRole.getRolePaths().add(rolePath);
                    }
                }
            }

            roleRepository.save(oldRole);
        }
    }

    @Transactional
    private void insertClients(String password) {
        for (String c: clients) {
            String[] s = c.split(":");
            String clientName = s[0];
            String[] clientRoles = s[1].split("\\s");
            Client oldClient = clientRepository.findOneByClientId(clientName);
            if (null == oldClient) {
                oldClient = new Client();
                oldClient.setClientId(clientName);
                oldClient.setAccessTokenValiditySeconds(691200); //token aktif 8 hari
                oldClient.setRefreshTokenValiditySeconds(7257600);
                oldClient.setGrantTypes("password refresh_token authorization_code");
                oldClient.setClientSecret(password);
                oldClient.setApproved(true);
                oldClient.setRedirectUris("");
                oldClient.setScopes("read write");
                List<Role> rls = roleRepository.findByNameIn(clientRoles);

                if (rls.size() > 0) {
                    oldClient.getAuthorities().addAll(rls);
                }
            }
            clientRepository.save(oldClient);
        }
    }

    @Transactional
    private void insertUser(String password) {
        for (String userNames: users) {
            String[] str = userNames.split(":");
            String username = str[0];
            String fullName = str[1];
            String[] roleNames = str[2].split("\\s");

            User oldUser = userRepository.findOneByUsername(username);
            if (null == oldUser) {
                oldUser = new User();
                oldUser.setUsername(username);
                oldUser.setPassword(password);
                List<Role> r = roleRepository.findByNameIn(roleNames);
                oldUser.setRoles(r);
            }
            Profile profile = new Profile();
            profile.setUser(oldUser);
            profile.setFullName(fullName);
            profile.setGender('M');
            profile.setPhone("08123456789");
            profile.setDob(LocalDate.of(1996, 2, 12));
            profile.setImgUrl(dataInitial.getImgbbUrl() + str[3]);

            profileRepository.save(profile);
            if (fullName.equals("Reza Rahadian")) {
                Creator creator = new Creator();
                creator.setUser(oldUser);
                creator.setOrganization("SYNRGY Academy");
                creator.setBankName("Bank Central Asia");
                creator.setBankAccountName(fullName);
                creator.setBankAccount("99977788812");
                creator.setBio("Pelaku Industri Kreatif");
                creator.setNik("3511231231230001");
                creator.setOccupation("Student");
                creator.setSocialMedia("Instagram");
                creator.setSocialMediaAccount("reza.rahadian");
                creatorRepository.save(creator);
            }

            Address address = new Address();
            address.setRecipient(fullName);
            address.setPhone("08123456789");
            address.setAddress("Jl. Pasar Kembang No. 221B Yogyakarta");
            address.setPostalCode(65784);
            address.setUser(oldUser);
            address.setMain(true);
            addressRepository.save(address);
        }
    }

}