package com.berbalik.asakarya.service;

import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.oauth.Oauth2UserDetailsService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserValidationService {

    @Autowired
    private Oauth2UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    public Long getUserLoginId(Principal principal) {

        Map map = new HashMap();

        String username = principal.getName();
        UserDetails user = null;

        // user detail validation
        if (!StringUtils.isEmpty(username)) {
            user = userDetailsService.loadUserByUsername(username);
        }

        // get user data from database
        User userLogin = userRepository.findOneByUsername(user.getUsername());

        return userLogin.getId();
    }
}
