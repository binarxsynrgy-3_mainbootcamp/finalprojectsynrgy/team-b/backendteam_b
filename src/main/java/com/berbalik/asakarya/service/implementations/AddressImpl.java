package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.AddressModel;
import com.berbalik.asakarya.entity.Address;
import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.AddressRepository;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.interfaces.AddressService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class AddressImpl implements AddressService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAllByUserId(Long userId) {
        try {
            boolean isUserExist = userRepository.existsById(userId);
            if (!isUserExist) return mapResponseTemplate.notFound("User");

            log.info("Get all address with user_id {}", userId);
            List<AddressModel> addressModels = new ArrayList<>();
            addressRepository.getAllByUserId(userId).forEach(address -> {
                addressModels.add(ModelConverter.toAddressModel(address));
            });

            return mapResponseTemplate.success(addressModels);
        } catch (Exception e) {
            log.info("Failed to get all address with user_id {}: {}", userId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            boolean isAddressExist = addressRepository.existsById(id);
            if (!isAddressExist) return mapResponseTemplate.notFound("Address");

            log.info("Get address with id {}", id);
            AddressModel addressModel = ModelConverter.toAddressModel(addressRepository.getById(id));

            return mapResponseTemplate.success(addressModel);
        } catch (Exception e) {
            log.info("Failed to get address with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getMain(Long userId) {
        try {
            boolean isUserExist = userRepository.existsById(userId);
            if (!isUserExist) return mapResponseTemplate.notFound("User");

            log.info("Get main address with user_id {}", userId);
            AddressModel addressModel = ModelConverter.toAddressModel(addressRepository.getMainAddress(userId));

            return mapResponseTemplate.success(addressModel);
        } catch (Exception e) {
            log.info("Failed to get main address with user_id {}: {}", userId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(AddressModel addressModel, Long userLoginId) {
        try {
            Address address = ModelConverter.toAddress(addressModel);
            User user = userRepository.getById(userLoginId);
            address.setUser(user);

            if (addressRepository.getAllByUserId(userLoginId).size() < 1) {
                address.setMain(true);
            }

            log.info("Save new address for user_id {}", userLoginId);
            Address address1 = addressRepository.save(address);

            return mapResponseTemplate.created(address1.getId());
        } catch (Exception e) {
            log.info("Failed to save new address for user_id {}: {}", userLoginId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(AddressModel addressModel, Long userLoginId) {
        try {
            boolean isUserExist = userRepository.existsById(userLoginId);
            if (!isUserExist) return mapResponseTemplate.notFound("User");

//            if (userLoginId != addressModel.getUserId()) return mapResponseTemplate.forbidden(null);

            Address addressExist = addressRepository.getById(addressModel.getId());
            addressExist.setRecipient(addressModel.getRecipient());
            addressExist.setPhone(addressModel.getPhone());
            addressExist.setAddress(addressModel.getAddress());
            addressExist.setPostalCode(addressModel.getPostalCode());

            addressRepository.save(addressExist);
            return mapResponseTemplate.updated();
        } catch (Exception e) {
            log.info("Failed to update address for user_id {}: {}", userLoginId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map setMainAddress(Long id, Long userLoginId) {
        try {
            Address address = addressRepository.getById(id);
            if (userLoginId != address.getUser().getId()) return mapResponseTemplate.forbidden(null);

            Address mainAddress = addressRepository.getMainAddress(userLoginId);
            mainAddress.setMain(false);
            addressRepository.save(mainAddress);

            log.info("Set address_id {} as main address for user_id {}", id, userLoginId);
            address.setMain(true);
            addressRepository.save(address);
            return mapResponseTemplate.updated();
        } catch (Exception e) {
            log.info("Failed to set address_id {} as main address for user_id {}: {}", id, userLoginId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id, Long userLoginId) {
        try {
            Address address = addressRepository.getById(id);
            if (userLoginId != address.getUser().getId()) return mapResponseTemplate.forbidden(null);

            if (address.isMain()) return mapResponseTemplate.forbidden("Can't delete main address, please change the main address to another address.");

            Date now = new Date();
            address.setDeletedAt(now);

            addressRepository.save(address);
            return mapResponseTemplate.deleted();
        } catch (Exception e) {
            log.info("Failed to delete address with address_id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
