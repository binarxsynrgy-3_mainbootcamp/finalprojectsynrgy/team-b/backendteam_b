package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.BookmarkModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Bookmark;
import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.CampaignRepository;
import com.berbalik.asakarya.repository.BookmarkRepository;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.interfaces.BookmarkService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageBookmark;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class BookmarkImpl implements BookmarkService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BookmarkRepository bookmarkRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map insert(BookmarkModel bookmarkModel, Long userLoginId) {
        try {
            User user = userRepository.getById(userLoginId);

            boolean isExist = campaignRepository.existsById(bookmarkModel.getCampaignId());
            if (!isExist) return mapResponseTemplate.notFound("Campaign");

            Bookmark bookmarkExist = bookmarkRepository.findOneByUserAndCampaignId(userLoginId, bookmarkModel.getCampaignId());
            if (bookmarkExist !=null) return mapResponseTemplate.forbidden("Campaign already bookmarked");

            Campaign campaign = campaignRepository.getById(bookmarkModel.getCampaignId());

            Bookmark bookmark = new Bookmark();
            bookmark.setUser(user);
            bookmark.setCampaign(campaign);

            log.info("Bookmark campaign {}", bookmarkModel.getId());
            Bookmark bookmark1 = bookmarkRepository.save(bookmark);
            return mapResponseTemplate.created(bookmark1.getId());
        } catch (Exception e){
            log.error("Failed to bookmark campaign {}: {}", bookmarkModel.getId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getAllByUserLoginId(Long userId, PageableModel pageableModel) {
        CustomPageBookmark<Bookmark> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            log.info("Get all bookmarked campaign with userId {}", userId);
            list = new CustomPageBookmark<Bookmark>(bookmarkRepository.getAllByUserId(userId, options));
            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all bookmarked campaign: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            boolean isExist = bookmarkRepository.existsById(id);
            if (!isExist) return mapResponseTemplate.notFound("Bookmarked Campaign");

            Bookmark bookmarkExist = bookmarkRepository.getById(id);

            Date now = new Date();
            bookmarkExist.setDeletedAt(now);

            log.info("Delete bookmarked campaign with id {}", id);
            bookmarkRepository.save(bookmarkExist);
            return mapResponseTemplate.deleted();
        } catch (Exception e){
            log.error("Failed to delete bookmarked campaign with id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
