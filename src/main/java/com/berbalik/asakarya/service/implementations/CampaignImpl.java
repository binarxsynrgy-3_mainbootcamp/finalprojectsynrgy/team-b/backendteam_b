package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.CampaignModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Category;
import com.berbalik.asakarya.entity.Creator;
import com.berbalik.asakarya.repository.CampaignRepository;
import com.berbalik.asakarya.repository.CategoryRepository;
import com.berbalik.asakarya.repository.CreatorRepository;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.interfaces.CampaignService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageCampaign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class CampaignImpl implements CampaignService {

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CreatorRepository creatorRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAll(PageableModel pageableModel, Long categoryId, String title) {
        CustomPageCampaign<Campaign> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            if (categoryId != null){
                boolean categoryIsExist = categoryRepository.existsById(categoryId);
                if (!categoryIsExist) return mapResponseTemplate.notFound("Category");

                if (title != null){
                    log.info("Get all verified campaigns by categoryId {} AND title {}", categoryId, title);
                    list = new CustomPageCampaign<Campaign>(campaignRepository.findAllByCategoryIdAndTitleLike(categoryId, "%" + title.toLowerCase() + "%", options));
                } else {
                    log.info("Get all verified campaigns by categoryId");
                    list = new CustomPageCampaign<Campaign>(campaignRepository.findAllByCategoryId(categoryId, options));
                }
            } else if (title != null){
                log.info("Get all verified campaigns by title {}", title);
                list = new CustomPageCampaign<Campaign>(campaignRepository.findAllByTitleLike("%" + title.toLowerCase() + "%", options));
            } else {
                log.info("Get all verified campaigns");
                list = new CustomPageCampaign<Campaign>(campaignRepository.findAll(options));
            }

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all verified campaigns: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(CampaignModel campaignModel, Long userLoginId) {
        try {
//            Long daysLeft = ChronoUnit.DAYS.between(LocalDate.now(), campaignModel.getDue());
//            if (daysLeft < 0) return mapResponseTemplate.forbidden("Days left lower than 0.");

            Creator creator = creatorRepository.getByUserId(userLoginId);
            if (creator == null) return mapResponseTemplate.notFound("Creator");

            boolean categoryIsExist = categoryRepository.existsById(campaignModel.getCategoryId());
            if (!categoryIsExist) return mapResponseTemplate.notFound("Category");
            Category category = categoryRepository.getById(campaignModel.getCategoryId());

            Campaign campaign = ModelConverter.toCampaign(campaignModel);
            campaign.setCreator(creator);
            campaign.setCategory(category);

            log.info("Save new campaign {}", campaignModel.getTitle());
            Campaign campaign1 = campaignRepository.save(campaign);
            return mapResponseTemplate.created(campaign1.getId());
        }  catch (Exception e){
            log.error("Failed to save new campaign {}: {}", campaignModel.getTitle(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(CampaignModel campaignModel, Long userLoginId) {
        try {
            Creator creator = creatorRepository.getByUserId(userLoginId);
            if (creator == null) return mapResponseTemplate.notFound("Creator");

            boolean categoryIsExist = categoryRepository.existsById(campaignModel.getCategoryId());
            if (!categoryIsExist) return mapResponseTemplate.notFound("Category");
            Category category = categoryRepository.getById(campaignModel.getCategoryId());

            Campaign campaignExist = campaignRepository.getById(campaignModel.getId());
            if (campaignExist == null) return mapResponseTemplate.notFound("Campaign");

            if (campaignExist.getCreator().getId() != creator.getId()) return mapResponseTemplate.forbidden(null);

            campaignExist.setTitle(campaignModel.getTitle());
            campaignExist.setDescription(campaignModel.getDescription());
            campaignExist.setFundAmount(campaignModel.getFundAmount());
            campaignExist.setDue(campaignModel.getDue());
            campaignExist.setImgUrl(campaignModel.getImgUrl());
            campaignExist.setWebsite(campaignModel.getWebsite());
            campaignExist.setLocation(campaignModel.getLocation());
            campaignExist.setCategory(category);

            log.info("Update campaign info with campaign_id {}", campaignExist.getId());
            campaignRepository.save(campaignExist);
            return mapResponseTemplate.updated();
        } catch (Exception e){
            log.error("Failed to update campaign info with campaign_id {}: {}", campaignModel.getId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            log.info("Get verified campaign info with id {}", id);
            Campaign campaignExist = campaignRepository.getById(id);
            if (campaignExist == null) return mapResponseTemplate.notFound("Campaign");

            CampaignModel campaignModel = ModelConverter.toCampaignModel(campaignExist);
            return mapResponseTemplate.success(campaignModel);
        } catch (Exception e){
            log.error("Failed to get verified campaign info with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getAllMonitoringByCreatorId(PageableModel pageableModel, Long userLoginId) {
        CustomPageCampaign<Campaign> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            log.info("Get monitoring campaign for creator_id {}", userLoginId);
            list = new CustomPageCampaign<Campaign>(campaignRepository.getAllMonitoringByCreatorId(userLoginId, options));

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all monitoring campaigns by creator_id {}: {}", userLoginId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getAllByCreatorId(Long creatorId, PageableModel pageableModel) {
        CustomPageCampaign<Campaign> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            log.info("Get verified campaign by creator_id {}", creatorId);
            list = new CustomPageCampaign<Campaign>(campaignRepository.findAllByCreatorId(creatorId, options));

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all verified campaigns by creator_id {}: {}", creatorId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id, Long userLoginId) {
        try {
            Creator creator = creatorRepository.getByUserId(userLoginId);
            if (creator == null) return mapResponseTemplate.notFound("Creator");

            Campaign campaignExist = campaignRepository.getById(id);
            if (campaignExist == null) return mapResponseTemplate.notFound("Campaign");
            if (campaignExist.getCreator().getId() != creator.getId()) return mapResponseTemplate.forbidden(null);

            Date now = new Date();
            campaignExist.setDeletedAt(now);

            log.info("Delete campaign info with id {}", id);
            campaignRepository.save(campaignExist);
            return mapResponseTemplate.deleted();
        } catch (Exception e){
            log.error("Failed to delete campaign info with id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map adminGetAll(PageableModel pageableModel) {
        CustomPageCampaign<Campaign> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            log.info("[Admin] Get all campaign");
            list = new CustomPageCampaign<Campaign>(campaignRepository.adminFindAll(options));
            return mapResponseTemplate.success(list);
        }catch (Exception e) {
            log.error("[Admin] Failed to get all campaign : {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map verify(Long id, boolean isVerified) {
        try {
            Campaign campaignExist = campaignRepository.getById(id);
            if (campaignExist == null) return mapResponseTemplate.notFound("Campaign");

            if (campaignExist.getStatus() == 1) return mapResponseTemplate.forbidden("Campaign already verified");

            int status = (isVerified) ? 1 : 2;
            campaignExist.setStatus(status);

            log.info("[Admin] Verify campaign {} with id {}", isVerified, id);
            campaignRepository.save(campaignExist);
            return mapResponseTemplate.updated();
        } catch (Exception e){
            log.error("[Admin] Failed to verify campaign with id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
