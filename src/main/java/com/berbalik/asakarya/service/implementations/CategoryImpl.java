package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.entity.Category;
import com.berbalik.asakarya.repository.CategoryRepository;
import com.berbalik.asakarya.service.interfaces.CategoryService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class CategoryImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAll() {
        try {
            log.info("Get all categories");
            List<Category> categories = categoryRepository.findAll();

            return mapResponseTemplate.success(categories);
        } catch (Exception e) {
            log.error("Failed to get all categories: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(Category category) {
        try {
            log.info("Save new category {}", category.getCategoryName());
            Category category1 = categoryRepository.save(category);

            return mapResponseTemplate.created(category1.getId());
        } catch (Exception e) {
            log.error("Failed to save new category {}: {}", category.getCategoryName(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(Category category) {
        try {
            boolean categoryIsExist = categoryRepository.existsById(category.getId());
            if (!categoryIsExist) return mapResponseTemplate.notFound("Category");

            Category categoryExist = categoryRepository.getById(category.getId());
            categoryExist.setCategoryName(category.getCategoryName());

            log.info("Update category {}", category.getCategoryName());
            categoryRepository.save(category);

            return mapResponseTemplate.updated();
        } catch (Exception e) {
            log.error("Failed to update category {}: {}", category.getCategoryName(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            boolean categoryIsExist = categoryRepository.existsById(id);
            if (!categoryIsExist) return mapResponseTemplate.notFound("Category");

            Category categoryExist = categoryRepository.getById(id);
            Date now = new Date();
            categoryExist.setDeletedAt(now);

            log.info("Delete category with id {}", id);
            categoryRepository.save(categoryExist);

            return mapResponseTemplate.deleted();
        } catch (Exception e) {
            log.error("Failed to delete category with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
