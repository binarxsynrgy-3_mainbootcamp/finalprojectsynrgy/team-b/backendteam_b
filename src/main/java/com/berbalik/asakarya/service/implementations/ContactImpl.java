package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.ContactModel;
import com.berbalik.asakarya.entity.Contact;
import com.berbalik.asakarya.repository.ContactRepository;
import com.berbalik.asakarya.service.interfaces.ContactService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class ContactImpl implements ContactService {

    @Autowired
    private ContactRepository contactRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAll() {
        try {
            log.info("Get all contact message");
            List<ContactModel> contactModels = new ArrayList<>();
            contactRepository.findAll().forEach(
                    contact -> contactModels.add(ModelConverter.toContactModel(contact))
            );

            return mapResponseTemplate.success(contactModels);
        } catch (Exception e) {
            log.error("Failed to get all contact message");
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            if (!contactRepository.existsById(id)) return mapResponseTemplate.notFound("Contact Message");

            log.info("Get contact info with id {}", id);
            ContactModel contactModel = ModelConverter.toContactModel(contactRepository.getById(id));

            return mapResponseTemplate.success(contactModel);
        } catch (Exception e) {
            log.error("Failed to get contact info with id {}: {}", id, e.getLocalizedMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(Contact contact) {
        try {
            log.info("Save new contact message from {}", contact.getEmail());
            Contact contact1 = contactRepository.save(contact);

            return mapResponseTemplate.created(contact1.getId());
        } catch (Exception e) {
            log.error("Failed to save new contact message from {}: {}", contact.getEmail(), e.getLocalizedMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
