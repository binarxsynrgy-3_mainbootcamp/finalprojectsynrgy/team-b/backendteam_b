package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.CreatorModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.entity.Creator;
import com.berbalik.asakarya.entity.oauth.Role;
import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.CreatorRepository;
import com.berbalik.asakarya.repository.oauth.RoleRepository;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.interfaces.CreatorService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageCreator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class CreatorImpl implements CreatorService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CreatorRepository creatorRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map insert(CreatorModel creatorModel, Long userLoginId) {
        try {
            boolean userIsExist = userRepository.existsById(userLoginId);
            if (!userIsExist) return mapResponseTemplate.notFound("User");

            // validate user login access
//            if (userLoginId != creatorModel.getId()) return mapResponseTemplate.forbidden(null);

            Creator creatorExist = creatorRepository.getByUserId(userLoginId);
            if (creatorExist != null) return mapResponseTemplate.forbidden("Creator already registered");

            User user = userRepository.getById(userLoginId);

            //add creator role
            Role creatorRole = roleRepository.findOneByName("ROLE_CREATOR");
            List<Role> roleList = user.getRoles();
            roleList.add(creatorRole);
            user.setRoles(roleList);

            Creator creator = new Creator();
            creator.setUser(user);
            creator.setOrganization(creatorModel.getOrganization());
            creator.setOccupation(creatorModel.getOccupation());
            creator.setSocialMedia(creatorModel.getSocialMedia());
            creator.setSocialMediaAccount(creatorModel.getSocialMediaAccount());
            creator.setBio(creatorModel.getBio());
            creator.setNik(creatorModel.getNik());
            creator.setKtpUrl(creatorModel.getKtpUrl());
            creator.setBankName(creatorModel.getBankName());
            creator.setBankAccount(creatorModel.getBankAccount());
            creator.setBankAccountName(creatorModel.getBankAccountName());

            log.info("Save new creator with user_id {}", userLoginId);
            userRepository.save(user);
            Creator creator1 = creatorRepository.save(creator);
            return mapResponseTemplate.created(creator1.getId());
        } catch (Exception e){
            log.error("Failed to save new creator with user_id {}: {}", userLoginId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(CreatorModel creatorModel, Long userLoginId) {
        try {
            Creator creatorExist = creatorRepository.getByUserId(userLoginId);
            if (creatorExist == null) return mapResponseTemplate.notFound("Creator");

//            // validate user login access
//            if (userLoginId != creatorExist.getId()) return mapResponseTemplate.forbidden(null);

            creatorExist.setOrganization(creatorModel.getOrganization());
            creatorExist.setOccupation(creatorModel.getOccupation());
            creatorExist.setSocialMedia(creatorModel.getSocialMedia());
            creatorExist.setSocialMediaAccount(creatorModel.getSocialMediaAccount());
            creatorExist.setBio(creatorModel.getBio());
            creatorExist.setNik(creatorModel.getNik());
            creatorExist.setKtpUrl(creatorModel.getKtpUrl());
            creatorExist.setBankName(creatorModel.getBankName());
            creatorExist.setBankAccount(creatorModel.getBankAccount());
            creatorExist.setBankAccountName(creatorModel.getBankAccountName());

            log.info("Update creator info with user_id {}", userLoginId);
            CreatorModel updatedCreator = ModelConverter.toCreatorModel(creatorRepository.save(creatorExist));
            return mapResponseTemplate.success(updatedCreator);
        } catch (Exception e){
            log.error("Failed to update creator info with user_id {}: {}", userLoginId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            log.info("Get creator info with user_id {}", id);
            Creator creatorExist = creatorRepository.getByUserId(id);
            if (creatorExist == null) return mapResponseTemplate.notFound("Creator");

            CreatorModel creatorModel = ModelConverter.toCreatorModel(creatorExist);
            return mapResponseTemplate.success(creatorModel);
        } catch (Exception e){
            log.error("Failed to get creator info with user_id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id, Long userLoginId) {
        try {
            Creator creatorExist = creatorRepository.getByUserId(id);
            if (creatorExist == null) return mapResponseTemplate.notFound("Creator");

            // validate user login access
            if (userLoginId != creatorExist.getId()) return mapResponseTemplate.forbidden(null);

            Date now = new Date();
            creatorExist.setDeletedAt(now);

            log.info("Delete creator info with user_id {}", id);
            creatorRepository.save(creatorExist);
            return mapResponseTemplate.deleted();
        } catch (Exception e){
            log.error("Failed to delete creator info with user_id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getAll(PageableModel pageableModel) {
        CustomPageCreator<Creator> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            log.info("Get all creators");
            list = new CustomPageCreator<Creator>(creatorRepository.findAll(options));
            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all creators: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
