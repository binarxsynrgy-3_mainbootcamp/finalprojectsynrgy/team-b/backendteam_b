package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.repository.*;
import com.berbalik.asakarya.service.interfaces.DashboardService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class DashboardImpl implements DashboardService {

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private CreatorRepository creatorRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private DashboardRepository dashboardRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map dashboard() {
        try {
            Map map = new HashMap();

            log.info("[Admin] Count of user");
            Long count = profileRepository.count();
            map.put("countUser", count);

            log.info("[Admin] Count of creator");
            Long countCreator = creatorRepository.count();
            map.put("countCreator", countCreator);

            log.info("[Admin] Count of campaign");
            Long countCampaign = campaignRepository.count();
            map.put("countCampaign", countCampaign);

            log.info("[Admin] Count of active campaign");
            Integer countActiveCampaign = dashboardRepository.countActiveCampaign();
            map.put("countActiveCampaign", countActiveCampaign);

            log.info("[Admin] Count of campaign finished");
            Integer countCampaignFinished = dashboardRepository.countCampaignFinished();
            map.put("countCampaignFinished", countCampaignFinished);

            log.info("[Admin] Sum of donation");
            Float sumDonation = dashboardRepository.sumDonation();
            map.put("sumDonation", sumDonation);

            log.info("[Admin] Sum of verified donation");
            Float sumVerifiedDonation = dashboardRepository.sumVerifiedDonation();
            map.put("sumVerifiedDonation", sumVerifiedDonation);

            log.info("[Admin] Count of gallery");
            Integer countGallery = dashboardRepository.countGallery();
            map.put("countGallery", countGallery);

            return mapResponseTemplate.success(map);
        } catch (Exception e) {
            log.error("[Admin] Failed to get dashboard info");
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

}
