package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.DisbursementModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Disbursement;
import com.berbalik.asakarya.repository.CampaignRepository;
import com.berbalik.asakarya.repository.DisbursementRepository;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.interfaces.DisbursementService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageDisbursement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class DisbursementImpl implements DisbursementService {
    @Autowired
    private DisbursementRepository disbursementRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private UserRepository userRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();


    @Override
    public Map getAllByCampaignId(PageableModel pageableModel, Long campaignId) {
        CustomPageDisbursement<Disbursement> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            Campaign campaign = campaignRepository.getById(campaignId);
            if (campaign == null) return mapResponseTemplate.notFound("Campaign");
            log.info("Get all disbursement with campaignId {}", campaignId);
            list = new CustomPageDisbursement<Disbursement>(disbursementRepository.findAllByCampaignId(campaignId, options));

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all disbursement with campaignId {}: {}", campaignId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            log.info("Get disbursement info with id {}", id);

            boolean isDisbursementExist = disbursementRepository.existsById(id);
            if (!isDisbursementExist) mapResponseTemplate.notFound("Disbursement");

            DisbursementModel disbursementModel = ModelConverter.toDisbursementModel(disbursementRepository.getById(id));

            return mapResponseTemplate.success(disbursementModel);
        } catch (Exception e) {
            log.error("Failed to get disbursement info with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map adminGetAll(PageableModel pageableModel) {
        CustomPageDisbursement<Disbursement> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            log.info("[Admin] Get all disbursement");
            list = new CustomPageDisbursement<Disbursement>(disbursementRepository.findAll(options));

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("[Admin] Failed to get all disbursement: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(DisbursementModel disbursementModel, Long userLoginId) {
        try {
            boolean isCampaignExist = campaignRepository.existsById(disbursementModel.getCampaignId());
            if (!isCampaignExist) return mapResponseTemplate.notFound("Campaign");

            Campaign campaign = campaignRepository.getById(disbursementModel.getCampaignId());
            float fee = (float)(campaign.getSumDonation() * 0.05);
            float fundAmount = campaign.getSumDonation() - fee;
            float remainingFunds = fundAmount - campaign.getSumDisbursement();
            log.info(
                    "sumDonation: {} || fee 5%: {} || fundAmount: {} || sumDisbursement: {} || remainingFunds: {} || disbursementAmount: {}",
                    campaign.getSumDonation(), fee, fundAmount, campaign.getSumDisbursement(), remainingFunds, disbursementModel.getAmount()
            );

            if (disbursementModel.getAmount() > remainingFunds){
                return mapResponseTemplate.forbidden("Disbursement amount greater than the remaining funds: " + remainingFunds);
            }
            Disbursement disbursement = ModelConverter.toDisbursement(disbursementModel);
            disbursement.setCampaign(campaign);
            disbursement.setUser(userRepository.getById(userLoginId));

            log.info("[Admin] Save new disbursement for campaign_id {}", disbursementModel.getCampaignId());
            Disbursement disbursement1 = disbursementRepository.save(disbursement);

            campaign.setSumDisbursement(campaign.getSumDisbursement() + disbursement1.getAmount());
            campaignRepository.save(campaign);
            return mapResponseTemplate.created(disbursement1.getId());
        } catch (Exception e){
            log.error("[Admin] Failed to save new disbursement for campaign_id {}: {}", disbursementModel.getCampaignId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(DisbursementModel disbursementModel) {
        try {
            boolean isDisbursementExist = disbursementRepository.existsById(disbursementModel.getId());
            if (!isDisbursementExist) return mapResponseTemplate.notFound("Disbursement");

            Disbursement disbursement = disbursementRepository.getById(disbursementModel.getId());
            disbursement.setTitle(disbursementModel.getTitle());
            disbursement.setDescription(disbursementModel.getDescription());
            disbursement.setPaymentSlipUrl(disbursementModel.getPaymentSlipUrl());

            log.info("[Admin] Update disbursement with id {}", disbursementModel.getId());
            disbursementRepository.save(disbursement);
            return mapResponseTemplate.updated();
        } catch (Exception e) {
            log.error("[Admin] Failed to update disbursement info with id {}: {}", disbursementModel.getId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            boolean isDisbursementExist = disbursementRepository.existsById(id);
            if (!isDisbursementExist) return mapResponseTemplate.notFound("Disbursement");

            Disbursement disbursement = disbursementRepository.getById(id);
            disbursement.setDeletedAt(new Date());
            log.info("[Admin] Delete disbursement id {}", id);
            disbursementRepository.save(disbursement);

            Campaign campaign = campaignRepository.getById(disbursement.getCampaign().getId());
            campaign.setSumDisbursement(campaign.getSumDisbursement()-disbursement.getAmount());
            campaignRepository.save(campaign);

            return mapResponseTemplate.deleted();
        } catch (Exception e) {
            log.info("[Admin] Failed to delete disbursement with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
