package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.DonationModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.entity.Address;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Donation;
import com.berbalik.asakarya.entity.Reward;
import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.AddressRepository;
import com.berbalik.asakarya.repository.CampaignRepository;
import com.berbalik.asakarya.repository.DonationRepository;
import com.berbalik.asakarya.repository.RewardRepository;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.interfaces.DonationService;
import com.berbalik.asakarya.util.DigitGenerator;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageDonation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Slf4j
@Service
@Transactional
public class DonationImpl implements DonationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private RewardRepository rewardRepository;

    @Autowired
    private DonationRepository donationRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAll(Long userId, Long campaignId, PageableModel pageableModel) {
        CustomPageDonation<Donation> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            if (campaignId != null) {
                Campaign campaign = campaignRepository.getById(campaignId);
                if (campaign == null) return mapResponseTemplate.notFound("Campaign");
                log.info("Get all verified donation with campaignId {}", campaignId);
                list = new CustomPageDonation<Donation>(donationRepository.findAllByCampaignId(campaignId, options));
            } else if (userId != null){
                boolean isUserExist = userRepository.existsById(userId);
                if (!isUserExist) return mapResponseTemplate.notFound("User");
                log.info("Get all donations with userId {}", userId);
                list = new CustomPageDonation<Donation>(donationRepository.findAllByUserId(userId, options));
            } else {
                log.info("Get all verified donation");
                list = new CustomPageDonation<Donation>(donationRepository.findAll(options));
            }

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all donations: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            log.info("Get donation info with id {}", id);
            Donation donationExist = donationRepository.getById(id);
            if (donationExist == null) return mapResponseTemplate.notFound("Donation");

            DonationModel donationModel = ModelConverter.toDonationModel(donationExist);
            return mapResponseTemplate.success(donationModel);
        } catch (Exception e){
            log.error("Failed to get donation info with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(DonationModel donationModel, Long userLoginId) {
        try {
            Address address = new Address();
            if (donationModel.getAddressId() != null) {
                boolean isAddressExist = addressRepository.existsById(donationModel.getAddressId());
                if (!isAddressExist) return mapResponseTemplate.notFound("Address");

                address = addressRepository.getById(donationModel.getAddressId());
                log.info("create donation: userId {} userLoginId {}", address.getUser().getId(), userLoginId);
                if (address.getUser().getId() != userLoginId) return mapResponseTemplate.forbidden(null);
            } else {
                address = null;
            }

            boolean isCampaignExist = campaignRepository.existsById(donationModel.getCampaignId());
            if (!isCampaignExist) return mapResponseTemplate.notFound("Campaign");
            Campaign campaign = campaignRepository.getById(donationModel.getCampaignId());

            Donation donation = ModelConverter.toDonation(donationModel);
            donation.setAmount(donationModel.getAmount() + DigitGenerator.getRandomNumberUsingNextInt(111, 500));
            donation.setAddress(address);
            donation.setCampaign(campaign);

            //reward
            Reward reward = new Reward();
            if (address != null) {
                if (donation.getAmount() >= 500000f){
                    log.info("Type 3 Amount {} campaign_id {}", donation.getAmount(), donationModel.getCampaignId());
                    reward = rewardRepository.getByRewardTypeAndCampaignId(3L, donationModel.getCampaignId());
                } else if (donation.getAmount() >= 250000f){
                    log.info("Type 2 Amount {} campaign_id {}", donation.getAmount(), donationModel.getCampaignId());
                    reward = rewardRepository.getByRewardTypeAndCampaignId(2L, donationModel.getCampaignId());
                } else if(donation.getAmount() >= 100000f ){
                    log.info("Type 1 Amount {} campaign_id {}", donation.getAmount(), donationModel.getCampaignId());
                    reward = rewardRepository.getByRewardTypeAndCampaignId(1L, donationModel.getCampaignId());
                } else {
                    reward = null;
                }
            } else {
                reward = null;
            }
            donation.setReward(reward);

            log.info("Save new donation with campaign_id {} and address_id {}", donationModel.getCampaignId(), donationModel.getAddressId());
            Donation donation1 = donationRepository.save(donation);
            return mapResponseTemplate.created(donation1.getId());
        } catch (Exception e){
            log.error("Failed to save new creator with campaign_id {} and address_id {}: {}", donationModel.getCampaignId(), donationModel.getAddressId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(DonationModel donationModel, Long userLoginId) {
        try {
            Donation donationExist = donationRepository.getById(donationModel.getId());
            if (donationExist == null) return mapResponseTemplate.notFound("Donation");

            if (donationExist.getStatus() == 1) return mapResponseTemplate.forbidden("donation already verified");

            donationExist.setPaymentSlipUrl(donationModel.getPaymentSlipUrl());

            log.info("Update payment slip donation with donation_id {}", donationExist.getId());
            donationRepository.save(donationExist);
            return mapResponseTemplate.updated();
        } catch (Exception e) {
            log.error("Failed to update payment slip donation info with donation_id {}: {}", donationModel.getId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map adminGetAll(Long campaignId, PageableModel pageableModel) {
        CustomPageDonation<Donation> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            if (campaignId != null) {
                Campaign campaign = campaignRepository.getById(campaignId);
                if (campaign == null) return mapResponseTemplate.notFound("Campaign");
                log.info("[Admin] Get all donations with campaignId {}", campaignId);
                list = new CustomPageDonation<Donation>(donationRepository.adminFindAllByCampaignId(campaignId, options));
            } else {
                log.info("[Admin] Get all donations");
                list = new CustomPageDonation<Donation>(donationRepository.adminFindAll(options));
            }

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("[Admin] Failed to get all donations: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map verify(Long id, boolean isVerified) {
        try {
            Donation donationExist = donationRepository.getById(id);
            if (donationExist == null) return mapResponseTemplate.notFound("Donation");

            if (donationExist.getStatus() == 1) return mapResponseTemplate.forbidden("donation already verified");

            int status = (isVerified) ? 1 : 2;

            log.info("Verify donation {} with id {}", isVerified, id);
            donationExist.setStatus(status);
            donationRepository.save(donationExist);

            if (status == 1) {
                log.info("Get campaign with id {}", donationExist.getCampaign().getId());
                Campaign campaign = campaignRepository.getById(donationExist.getCampaign().getId());
                campaign.setSumDonation(campaign.getSumDonation() + donationExist.getAmount());
                campaign.setCountDonation(campaign.getCountDonation() + 1);

                log.info("Calculate donation for campaignId {}", campaign.getId());
                campaignRepository.save(campaign);
            }

            return mapResponseTemplate.updated();
        } catch (Exception e){
            log.error("Failed to verify donation with id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
