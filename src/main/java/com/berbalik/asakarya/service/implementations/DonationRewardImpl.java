package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.DonationModel;
import com.berbalik.asakarya.dto.DonationRewardModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Donation;
import com.berbalik.asakarya.repository.CampaignRepository;
import com.berbalik.asakarya.repository.DonationRepository;
import com.berbalik.asakarya.service.interfaces.DonationRewardService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageDonationReward;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class DonationRewardImpl implements DonationRewardService {

    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAllByCampaignId(Long campaignId, PageableModel pageableModel) {
        CustomPageDonationReward<Donation> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            Campaign campaign = campaignRepository.getById(campaignId);
            if (campaign == null) return mapResponseTemplate.notFound("Campaign");
            log.info("Get all donation reward with campaignId {}", campaignId);
            list = new CustomPageDonationReward<Donation>(donationRepository.findAllDonationRewardByCampaignId(campaignId, options));
            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all donation reward with campaignId {} : {}",campaignId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            log.info("Get donation reward with id {}", id);
            Donation donationExist = donationRepository.getById(id);
            if (donationExist == null) return mapResponseTemplate.notFound("Donation");

            DonationRewardModel donationRewardModel = ModelConverter.toDonationRewardModel(donationExist);
            return mapResponseTemplate.success(donationRewardModel);
        }catch (Exception e){
            log.error("Failed to get donation reward with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map deliver(Long id) {
        try {
            log.info("deliver donation reward with id {}", id);
            Donation donationExist = donationRepository.getById(id);
            if (donationExist == null) return mapResponseTemplate.notFound("Donation");

            if (donationExist.isDelivered()) return mapResponseTemplate.forbidden("Reward already delivered");
            donationExist.setDelivered(true);
            donationRepository.save(donationExist);
            return mapResponseTemplate.updated();
        } catch (Exception e) {
            log.error("Failed to deliver donation reward with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
