package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.FaqModel;
import com.berbalik.asakarya.dto.HistoryModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Faq;
import com.berbalik.asakarya.repository.CampaignRepository;
import com.berbalik.asakarya.repository.FaqRepository;
import com.berbalik.asakarya.service.interfaces.FaqService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class FaqImpl implements FaqService {

    @Autowired
    private FaqRepository faqRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAllByCampaignId(Long campaignId){
        try {
            Campaign campaign = campaignRepository.getById(campaignId);
            if (campaign==null) return mapResponseTemplate.notFound("Campaign");

            log.info("Get all faq of campaign_id {}", campaignId);
            List<FaqModel> faqModels = new ArrayList<>();
            faqRepository.getAllByCampaignId(campaignId).forEach(faq -> {
                faqModels.add(ModelConverter.toFaqModel(faq));
            });

            return mapResponseTemplate.success(faqModels);
        } catch (Exception e) {
            log.error("Failed to get all faq of campaign_id {}: {}", campaignId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            boolean isFaqExist = faqRepository.existsById(id);
            if (!isFaqExist) return mapResponseTemplate.notFound("FAQ");

            log.info("Get campaign FAQ detail with id {}", id);
            FaqModel faqModel = ModelConverter.toFaqModel(faqRepository.getById(id));

            return mapResponseTemplate.success(faqModel);
        } catch (Exception e) {
            log.error("Failed to get campaign FAQ with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(FaqModel faqModel) {
        try {
            Campaign campaign = campaignRepository.getById(faqModel.getCampaignId());
            if (campaign==null) return mapResponseTemplate.notFound("Campaign");

            Faq faq = ModelConverter.toFaq(faqModel);
            faq.setCampaign(campaign);

            log.info("Save new campaign faq {}", faqModel.getQuestion());
            Faq faq1 = faqRepository.save(faq);
            return mapResponseTemplate.created(faq1.getId());
        } catch (Exception e) {
            log.error("Failed to save new campaign faq: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(FaqModel faqModel) {
        try {
            boolean isFaqExist = faqRepository.existsById(faqModel.getId());
            if (!isFaqExist) return mapResponseTemplate.notFound("Faq");

            Faq faq = faqRepository.getById(faqModel.getId());
            faq.setQuestion(faqModel.getQuestion());
            faq.setAnswer(faqModel.getAnswer());

            log.info("Update campaign faq with id {}", faqModel.getId());
            faqRepository.save(faq);
            return mapResponseTemplate.updated();
        } catch (Exception e) {
            log.error("Failed to update campaign faq with id {}: {}", faqModel.getId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            boolean isFaqExist = faqRepository.existsById(id);
            if (!isFaqExist) return mapResponseTemplate.notFound("Faq");

            Faq faq = faqRepository.getById(id);
            faq.setDeletedAt(new Date());

            log.info("Delete campaign faq detail with id {}", id);
            faqRepository.save(faq);
            return mapResponseTemplate.deleted();
        } catch (Exception e) {
            log.error("Failed to delete campaign faq with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
