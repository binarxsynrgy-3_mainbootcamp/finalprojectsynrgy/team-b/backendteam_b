package com.berbalik.asakarya.service.implementations;


import com.berbalik.asakarya.dto.CampaignModel;
import com.berbalik.asakarya.dto.GalleryModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Category;
import com.berbalik.asakarya.entity.Creator;
import com.berbalik.asakarya.entity.Gallery;
import com.berbalik.asakarya.repository.CategoryRepository;
import com.berbalik.asakarya.repository.CreatorRepository;
import com.berbalik.asakarya.repository.GalleryRepository;
import com.berbalik.asakarya.service.interfaces.GalleryService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageCampaign;
import com.berbalik.asakarya.util.pagination.CustomPageGallery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class GalleryImpl implements GalleryService {

    @Autowired
    private CreatorRepository creatorRepository;

    @Autowired
    private GalleryRepository galleryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAll(PageableModel pageableModel, Long categoryId, String title) {
        CustomPageGallery<Gallery> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            if (categoryId != null){
                boolean categoryIsExist = categoryRepository.existsById(categoryId);
                if (!categoryIsExist) return mapResponseTemplate.notFound("Category");

                if (title != null){
                    log.info("Get all galleries by categoryId {} AND title {}", categoryId, title);
                    list = new CustomPageGallery<Gallery>(galleryRepository.findAllByCategoryIdAndTitleLike(categoryId, "%" + title.toLowerCase() + "%", options));
                } else {
                    log.info("Get all galleries by categoryId");
                    list = new CustomPageGallery<Gallery>(galleryRepository.findAllByCategoryId(categoryId, options));
                }
            } else if (title != null){
                log.info("Get all galleries by title {}", title);
                list = new CustomPageGallery<Gallery>(galleryRepository.findAllByTitleLike("%" + title.toLowerCase() + "%", options));
            } else {
                log.info("Get all galleries");
                list = new CustomPageGallery<Gallery>(galleryRepository.findAll(options));
            }

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all galleries: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getAllByCreatorId(Long creatorId, PageableModel pageableModel) {
        CustomPageGallery<Gallery> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            Creator creator = creatorRepository.getByUserId(creatorId);
            if (creator == null) return mapResponseTemplate.notFound("Creator");

            log.info("Get all galleries with creatorId {}", creatorId);
            list = new CustomPageGallery<Gallery>(galleryRepository.findAllByCreatorId(creatorId, options));

            return mapResponseTemplate.success(list);
        } catch (Exception e){
            log.error("Failed to get all galleries with creatorId {}: {}", creatorId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            log.info("Get gallery info with id {}", id);
            Gallery galleryExist = galleryRepository.getById(id);
            if (galleryExist == null) return mapResponseTemplate.notFound("Gallery");

            GalleryModel galleryModel = ModelConverter.toGalleryModel(galleryExist);
            return mapResponseTemplate.success(galleryModel);
        } catch (Exception e){
            log.error("Failed to get gallery info with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(GalleryModel galleryModel, Long userLoginId) {
        try {
            Creator creator = creatorRepository.getByUserId(userLoginId);
            if (creator == null) return mapResponseTemplate.notFound("Creator");

            boolean categoryIsExist = categoryRepository.existsById(galleryModel.getCategoryId());
            if (!categoryIsExist) return mapResponseTemplate.notFound("Category");
            Category category = categoryRepository.getById(galleryModel.getCategoryId());

            Gallery gallery = ModelConverter.toGallery(galleryModel);
            gallery.setCreator(creator);
            gallery.setCategory(category);

            log.info("Save new gallery {}", galleryModel.getTitle());
            Gallery gallery1 = galleryRepository.save(gallery);
            return mapResponseTemplate.created(gallery1.getId());
        } catch (Exception e) {
            log.error("Failed to save new gallery {}: {}", galleryModel.getTitle(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(GalleryModel galleryModel, Long userLoginId) {
        try {
            Gallery galleryExist = galleryRepository.getById(galleryModel.getId());
            if (galleryExist == null) return mapResponseTemplate.notFound("Gallery");

            Creator creator = creatorRepository.getByUserId(userLoginId);
            if (creator == null) return mapResponseTemplate.notFound("Creator");

            if (galleryExist.getCreator().getId() != creator.getId()) return mapResponseTemplate.forbidden(null);

            boolean categoryIsExist = categoryRepository.existsById(galleryModel.getCategoryId());
            if (!categoryIsExist) return mapResponseTemplate.notFound("Category");
            Category category = categoryRepository.getById(galleryModel.getCategoryId());

            galleryExist.setTitle(galleryModel.getTitle());
            galleryExist.setDescription(galleryModel.getDescription());
            galleryExist.setImgUrl1(galleryModel.getImgUrl1());
            galleryExist.setImgUrl2(galleryModel.getImgUrl2());
            galleryExist.setImgUrl3(galleryModel.getImgUrl3());
            galleryExist.setCategory(category);

            log.info("Update gallery info with galleryId {}", galleryExist.getId());
            galleryRepository.save(galleryExist);
            return mapResponseTemplate.updated();
        } catch (Exception e){
            log.error("Failed to update gallery info with galleryId {}: {}", galleryModel.getId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id, Long userLoginId) {
        try {
            Creator creator = creatorRepository.getByUserId(userLoginId);
            if (creator == null) return mapResponseTemplate.notFound("Creator");

            Gallery galleryExist = galleryRepository.getById(id);
            if (galleryExist == null) return mapResponseTemplate.notFound("Campaign");

            if (galleryExist.getCreator().getId() != creator.getId()) return mapResponseTemplate.forbidden(null);

            galleryExist.setDeletedAt(new Date());

            log.info("Delete gallery with id {}", id);
            galleryRepository.save(galleryExist);
            return mapResponseTemplate.deleted();
        } catch (Exception e){
            log.error("Failed to delete gallery with id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
