package com.berbalik.asakarya.service.implementations;


import com.berbalik.asakarya.dto.HistoryModel;
import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.entity.Bookmark;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.History;
import com.berbalik.asakarya.repository.CampaignRepository;
import com.berbalik.asakarya.repository.HistoryRepository;
import com.berbalik.asakarya.service.interfaces.HistoryService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageBookmark;
import com.berbalik.asakarya.util.pagination.CustomPageHistory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class HistoryImpl implements HistoryService {

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAllByCampaignId(Long campaignId, PageableModel pageableModel) {
        CustomPageHistory<History> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            Campaign campaignExist = campaignRepository.getById(campaignId);
            if (campaignExist == null) return mapResponseTemplate.notFound("Campaign");

            log.info("Get all history of campaign_id {}", campaignId);
            list = new CustomPageHistory<History>(historyRepository.getAllByCampaignId(campaignId, options));

            return mapResponseTemplate.success(list);
        } catch (Exception e) {
            log.error("Failed to get all history of campaign_id {}: {}", campaignId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            boolean isHistoryExist = historyRepository.existsById(id);
            if (!isHistoryExist) return mapResponseTemplate.notFound("History");

            log.info("Get campaign history detail with id {}", id);
            HistoryModel historyModel = ModelConverter.toHistoryModel(historyRepository.getById(id));

            return mapResponseTemplate.success(historyModel);
        } catch (Exception e) {
            log.error("Failed to get campaign history with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(HistoryModel historyModel) {
        try {
            boolean isCampaignExist = campaignRepository.existsById(historyModel.getCampaignId());
            if (!isCampaignExist) return mapResponseTemplate.notFound("Campaign");

            Campaign campaign = campaignRepository.getById(historyModel.getCampaignId());
            History history = ModelConverter.toHistory(historyModel);
            history.setCampaign(campaign);

            log.info("Save new campaign history {}", historyModel.getTitle());
            History history1 = historyRepository.save(history);
            return mapResponseTemplate.created(history1.getId());
        } catch (Exception e) {
            log.error("Failed to save new campaign history: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(HistoryModel historyModel) {
        try {
            boolean isHistoryExist = historyRepository.existsById(historyModel.getId());
            if (!isHistoryExist) return mapResponseTemplate.notFound("History");

            History history = historyRepository.getById(historyModel.getId());
            history.setTitle(historyModel.getTitle());
            history.setActivity(historyModel.getActivity());
            history.setImgUrl(historyModel.getImgUrl());

            log.info("Update campaign history with id {}", historyModel.getId());
            historyRepository.save(history);
            return mapResponseTemplate.updated();
        } catch (Exception e) {
            log.error("Failed to update campaign history with id {}: {}", historyModel.getId(), e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            boolean isHistoryExist = historyRepository.existsById(id);
            if (!isHistoryExist) return mapResponseTemplate.notFound("History");

            History history = historyRepository.getById(id);
            history.setDeletedAt(new Date());

            log.info("Delete campaign history detail with id {}", id);
            historyRepository.save(history);
            return mapResponseTemplate.deleted();
        } catch (Exception e) {
            log.error("Failed to delete campaign history with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getAll(PageableModel pageableModel) {
        CustomPageHistory<History> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            log.info("[Admin] Get all history");
            list = new CustomPageHistory<History>(historyRepository.findAll(options));

            return mapResponseTemplate.success(list);
        } catch (Exception e) {
            log.error("[Admin] Failed to get all history of campaign_id: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
