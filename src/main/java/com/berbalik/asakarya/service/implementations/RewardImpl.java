package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.RewardModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Reward;
import com.berbalik.asakarya.entity.RewardType;
import com.berbalik.asakarya.repository.CampaignRepository;
import com.berbalik.asakarya.repository.RewardRepository;
import com.berbalik.asakarya.repository.RewardTypeRepository;
import com.berbalik.asakarya.service.interfaces.RewardService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class RewardImpl implements RewardService {

    @Autowired
    private RewardRepository rewardRepository;

    @Autowired
    private RewardTypeRepository rewardTypeRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    private MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAllByCampaignId(Long campaignId) {
        try {
            if (campaignRepository.getById(campaignId)==null) return mapResponseTemplate.notFound("Campaign");

            log.info("Get all reward with campaign_id {}", campaignId);
            List<RewardModel> rewardModels = new ArrayList<>();
            rewardRepository.getAllByCampaignId(campaignId).forEach(reward -> {
                rewardModels.add(ModelConverter.toRewardModel(reward));
            });

            return mapResponseTemplate.success(rewardModels);
        } catch (Exception e) {
            log.error("Failed to get all reward with campaign_id {}: {}", campaignId, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            log.info("Get reward with id {}", id);
            boolean rewardIsExist = rewardRepository.existsById(id);
            if (!rewardIsExist) return mapResponseTemplate.notFound("Reward");

            Reward rewardExist = rewardRepository.getById(id);
            RewardModel rewardModel = ModelConverter.toRewardModel(rewardExist);
            return mapResponseTemplate.success(rewardModel);
        } catch (Exception e){
            log.error("Failed to get reward info with id {}: {}", id, e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map insert(List<RewardModel> rewardModels) {
        try {
            List<Long> ids = new ArrayList<>();
            for (RewardModel rewardModel: rewardModels) {
                boolean isRewardTypeExist = rewardTypeRepository.existsById(rewardModel.getRewardTypeId());
                if (!isRewardTypeExist) return mapResponseTemplate.notFound("Reward Type");

                Campaign campaign = campaignRepository.getById(rewardModel.getCampaignId());
                if (campaign==null) return mapResponseTemplate.notFound("Campaign");
//            if (campaign.getStatus()==1) return mapResponseTemplate.forbidden("Campaign already verified");

                Reward rewardExist = rewardRepository.getByRewardTypeAndCampaignId(rewardModel.getRewardTypeId(), rewardModel.getCampaignId());
                if (rewardExist != null) return mapResponseTemplate.forbidden("Reward for this type already exist.");

                Reward reward = ModelConverter.toReward(rewardModel);
                reward.setRewardType(rewardTypeRepository.getById(rewardModel.getRewardTypeId()));
                reward.setCampaign(campaign);

                log.info("Save new reward with campaign_id {}", rewardModel.getCampaignId());
                log.info("{}", reward.getImgUrl());
                Reward reward1 = rewardRepository.save(reward);
                ids.add(reward1.getId());
            }
            return mapResponseTemplate.created(ids);
        } catch (Exception e) {
            log.error("Failed to save new reward: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map update(List<RewardModel> rewardModels) {
        try {
//            boolean rewardTypeIsExist = rewardTypeRepository.existsById(rewardModel.getRewardTypeId());
//            if (!rewardTypeIsExist) return mapResponseTemplate.notFound("Reward Type");
//            RewardType rewardType = rewardTypeRepository.getById(rewardModel.getRewardTypeId());

//            Campaign campaign = campaignRepository.getById(rewardModel.getCampaignId());
//            if (campaign==null) return mapResponseTemplate.notFound("Campaign");
//            if (campaign.getStatus()==1) return mapResponseTemplate.forbidden("Campaign already verified");

            for (RewardModel rewardModel: rewardModels) {
                Reward rewardExist = rewardRepository.getById(rewardModel.getId());
                if (rewardExist == null) return mapResponseTemplate.notFound("Reward");

                rewardExist.setItem(rewardModel.getItem());
                rewardExist.setLimited(rewardModel.isLimited());
                rewardExist.setImgUrl(rewardModel.getImgUrl());

                log.info("Update reward info with reward_id {}", rewardExist.getId());
                rewardRepository.save(rewardExist);
            }
            return mapResponseTemplate.updated();
        } catch (Exception e){
            log.error("Failed to update reward info: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map deleteAllByCampaignId(Long id) {
        try {
            Campaign campaign = campaignRepository.getById(id);
            if (campaign==null) return mapResponseTemplate.notFound("Campaign");

            List<Reward> rewards = rewardRepository.getAllByCampaignId(id);
            for (Reward reward: rewards) {
                Reward rewardExist = rewardRepository.getById(reward.getId());
                Date now = new Date();
                rewardExist.setDeletedAt(now);

                log.info("Delete reward with id {}", reward.getId());
                rewardRepository.save(rewardExist);
            }

            return mapResponseTemplate.deleted();
        } catch (Exception e){
            log.error("Failed to delete reward with campaign_id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
