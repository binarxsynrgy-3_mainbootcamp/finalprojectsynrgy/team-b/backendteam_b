package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.entity.RewardType;
import com.berbalik.asakarya.repository.RewardTypeRepository;
import com.berbalik.asakarya.service.interfaces.RewardTypeService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class RewardTypeImpl implements RewardTypeService {

    @Autowired
    private RewardTypeRepository rewardTypeRepository;

    private MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map getAll() {
        try {
            log.info("Get all reward type info");
            List<RewardType> list = rewardTypeRepository.findAll();
            return mapResponseTemplate.success(list);
        } catch (Exception e) {
            log.error("Failed to get all reward type info: {}", e.getMessage());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
