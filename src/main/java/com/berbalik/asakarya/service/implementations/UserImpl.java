package com.berbalik.asakarya.service.implementations;

import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.dto.UpdatePasswordModel;
import com.berbalik.asakarya.util.Pagination;
import com.berbalik.asakarya.util.pagination.CustomPageProfile;
import com.berbalik.asakarya.dto.UserModel;
import com.berbalik.asakarya.entity.Profile;
import com.berbalik.asakarya.entity.oauth.Role;
import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.ProfileRepository;
import com.berbalik.asakarya.repository.oauth.RoleRepository;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import com.berbalik.asakarya.service.interfaces.UserService;
import com.berbalik.asakarya.util.MapResponseTemplate;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@Transactional
public class UserImpl implements UserService {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    RestTemplateBuilder restTemplateBuilder;

    RestTemplate restTemplate = new RestTemplate();

    MapResponseTemplate mapResponseTemplate = new MapResponseTemplate();

    @Override
    public Map register(UserModel userModel, boolean isAdmin) {
        try {
            String[] roleNames;
            if (isAdmin) {
                roleNames = new String[]{"ROLE_ADMIN","ROLE_CREATOR","ROLE_INVESTOR", "ROLE_READ", "ROLE_WRITE"};
            } else {
                roleNames = new String[]{"ROLE_INVESTOR", "ROLE_READ", "ROLE_WRITE"};
            }

            //Role validation
            List<Role> roles = roleRepository.findByNameIn(roleNames);
            if (roles.size() < 0) return mapResponseTemplate.notFound("Roles");

            //Username validation
            User usernameExist = userRepository.findOneByUsername(userModel.getUsername());
            if (usernameExist != null) return mapResponseTemplate.forbidden("Username already taken");

            //Encode Password
            String password = passwordEncoder.encode(userModel.getPassword()).replaceAll("\\s+", "");

            //Insert table user
            User user = new User();
            user.setPassword(password);
            user.setUsername(userModel.getUsername());
            user.setRoles(roles);

            //Insert table profile
            Profile profile = new Profile();
            profile.setFullName(userModel.getFullName());
            profile.setPhone(userModel.getPhone());
            profile.setUser(user);

            log.info("Create new user {}", userModel.getUsername());
            Profile profile1 = profileRepository.save(profile);
            return mapResponseTemplate.created(profile1.getId());
        } catch (Exception e){
            log.error("Failed to create new user {}", userModel.getUsername());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map login(String baseUrl, UserModel userModel) {
        try {
            String url = baseUrl + "/oauth/token";

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("username", userModel.getUsername());
            map.add("password", userModel.getPassword());
            map.add("grant_type", "password");
            map.add("client_id", "asakarya-app");
            map.add("client_secret", "password");

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);

            log.info("Rest Template: user login: {}", userModel.getUsername());
            String body = response.getBody()
                    .substring(1, response.getBody().length()-1)
                    .replaceAll("\"", "");
            String[] keyValuePairs = body.split(",");
            String token = keyValuePairs[0].split(":")[1];

            log.info("status code:{}", response.getStatusCode());
            return mapResponseTemplate.success(token);
        } catch (Exception e){
            log.error("Rest Template: user login failed {}", e.getMessage());
            return mapResponseTemplate.badRequest();
        }
    }

    @Override
    public Map update(UserModel userModel, Long userLoginId) {
        try {
            boolean isUserExist = userRepository.existsById(userLoginId);
            if (!isUserExist) return mapResponseTemplate.notFound("User");

            User user = userRepository.getById(userLoginId);
            // validate username exist
            if (userModel.getUsername() != null) {
                User userExist = userRepository.findOneByUsername(userModel.getUsername());
                if ((userExist != null) && (userExist.getId() != userLoginId)) return mapResponseTemplate.forbidden("Username already taken");

                user.setUsername(userModel.getUsername());
            }

            Profile profile = profileRepository.getById(userLoginId);
            profile.setFullName(userModel.getFullName());
            profile.setPhone(userModel.getPhone());
            profile.setGender(userModel.getGender());
            profile.setDob(userModel.getDob());
            profile.setImgUrl(userModel.getImgUrl());

            log.info("Update user profile with user_id {} username {}", userLoginId, userModel.getUsername());
            userRepository.save(user);
            profileRepository.save(profile);

            return mapResponseTemplate.updated();
        } catch (Exception e){
            log.error("Failed to update user profile with user_id {} username {}", userLoginId, userModel.getUsername());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getById(Long id) {
        try {
            boolean isProfileExist = profileRepository.existsById(id);
            if (!isProfileExist) return mapResponseTemplate.notFound("User");

            log.info("Get user info with user_id {}", id);
            UserModel user = ModelConverter.toUserModel(profileRepository.getById(id));

            return mapResponseTemplate.success(user);
        } catch (Exception e){
            log.error("Failed to get user info with user_id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map delete(Long id) {
        try {
            boolean isUserExist = userRepository.existsById(id);
            if (!isUserExist) return mapResponseTemplate.notFound("User");

            //soft delete
            Date now = new Date();

            User user = userRepository.getById(id);
            user.setEnabled(false);
            user.setDeletedAt(now);

            Profile profile = profileRepository.getById(id);
            profile.setDeletedAt(now);
            profile.setUser(user);

            log.info("Delete user with user_id {}", id);
            profileRepository.save(profile);
            return mapResponseTemplate.deleted();
        } catch (Exception e){
            log.error("Failed to delete user with user_id {}", id);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map updatePassword(UpdatePasswordModel updatePasswordModel, Long userLoginId) {
        try {
            boolean isUserExist = userRepository.existsById(userLoginId);
            if (!isUserExist) return mapResponseTemplate.notFound("User");

            User user = userRepository.getById(userLoginId);
            if (!passwordEncoder.matches(updatePasswordModel.getOldPassword(), user.getPassword())) return mapResponseTemplate.forbidden("Password doesn't match.");

            String password = passwordEncoder.encode(updatePasswordModel.getNewPassword()).replaceAll("\\s+", "");
            user.setPassword(password);

            log.info("Update user password with user_id {}", userLoginId);
            userRepository.save(user);
            return mapResponseTemplate.updated();
        } catch (Exception e){
            log.error("Failed to update user password with user_id {}", userLoginId);
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map getAll(PageableModel pageableModel) {
        CustomPageProfile<Profile> list;
        Pageable options;
        try {
            options = Pagination.pageable(pageableModel);

            log.info("Get all user info");
            list = new CustomPageProfile<Profile>(profileRepository.findAll(options));
            return mapResponseTemplate.success(list);
        }  catch (Exception e){
            log.error("Failed to get all user info");
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }

    @Override
    public Map resetPassword(UserModel userModel) {
        try {
            boolean isUserExist = userRepository.existsById(userModel.getId());
            if (!isUserExist) return mapResponseTemplate.notFound("User");

            String password = passwordEncoder.encode(userModel.getPassword()).replaceAll("\\s+", "");

            User user = userRepository.getById(userModel.getId());
            user.setPassword(password);

            log.info("[Admin] Reset password user with user_id {}", userModel.getId());
            userRepository.save(user);
            return mapResponseTemplate.updated();
        } catch (Exception e){
            log.error("[Admin] Failed to reset password user with user_id {}", userModel.getId());
            return mapResponseTemplate.internalServerError(e.getLocalizedMessage());
        }
    }
}
