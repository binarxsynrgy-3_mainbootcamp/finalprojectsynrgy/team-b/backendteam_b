package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.AddressModel;

import java.util.Map;

public interface AddressService {

    public Map getAllByUserId(Long userId);

    public Map getById(Long id);

    public Map getMain(Long userId);

    public Map insert(AddressModel addressModel, Long userLoginId);

    public Map update(AddressModel addressModel, Long userLoginId);

    public Map setMainAddress(Long id, Long userLoginId);

    public Map delete(Long id, Long userLoginId);
}
