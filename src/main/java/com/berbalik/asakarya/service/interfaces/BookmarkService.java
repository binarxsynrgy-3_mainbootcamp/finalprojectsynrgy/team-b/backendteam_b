package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.BookmarkModel;
import com.berbalik.asakarya.dto.PageableModel;

import java.util.Map;

public interface BookmarkService {

    public Map insert(BookmarkModel bookmarkModel, Long userLoginId);

    public Map getAllByUserLoginId(Long userId, PageableModel pageableModel);

    public Map delete(Long id);
}
