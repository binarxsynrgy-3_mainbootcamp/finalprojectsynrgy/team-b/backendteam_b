package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.CampaignModel;
import com.berbalik.asakarya.dto.PageableModel;

import java.util.Map;

public interface CampaignService {

    public Map getAll(PageableModel pageableModel, Long categoryId, String title);

    public Map getAllByCreatorId(Long creatorId, PageableModel pageableModel);

    public Map getAllMonitoringByCreatorId(PageableModel pageableModel, Long userLoginId);

    public Map insert(CampaignModel campaignModel, Long userLoginId);

    public Map update(CampaignModel campaignModel, Long userLoginId);

    public Map getById(Long id);

    public Map delete(Long id, Long userLoginId);

    // ADMIN ROLE
    public Map adminGetAll(PageableModel pageableModel);

    public Map verify(Long id, boolean isVerified);
}
