package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.entity.Category;

import java.util.Map;

public interface CategoryService {

    public Map getAll();

    // admin role
    public Map insert(Category category);

    public Map update(Category category);

    public Map delete(Long id);
}
