package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.entity.Contact;

import java.util.Map;

public interface ContactService {

    public Map getAll();

    public Map getById(Long id);

    public Map insert(Contact contact);
}
