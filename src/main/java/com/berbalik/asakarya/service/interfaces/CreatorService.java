package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.CreatorModel;
import com.berbalik.asakarya.dto.PageableModel;

import java.util.Map;

public interface CreatorService {

    public Map insert(CreatorModel creatorModel, Long userLoginId);

    public Map update(CreatorModel creatorModel, Long userLoginId);

    public Map getById(Long id);

    public Map delete(Long id, Long userLoginId);

    // ADMIN ROLE
    public Map getAll(PageableModel pageableModel);
}
