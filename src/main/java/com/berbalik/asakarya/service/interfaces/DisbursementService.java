package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.DisbursementModel;
import com.berbalik.asakarya.dto.PageableModel;

import java.util.Map;

public interface DisbursementService {

    public Map getAllByCampaignId(PageableModel pageableModel, Long campaignId);

    public Map getById(Long id);

    // ADMIN ROLE
    public Map adminGetAll(PageableModel pageableModel);

    public Map insert(DisbursementModel disbursementModel, Long userLoginId);

    public Map update(DisbursementModel disbursementModel);

    public Map delete(Long id);
}
