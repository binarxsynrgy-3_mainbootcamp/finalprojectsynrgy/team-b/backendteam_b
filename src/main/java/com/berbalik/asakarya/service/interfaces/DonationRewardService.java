package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.PageableModel;

import java.util.Map;

public interface DonationRewardService {

    public Map getAllByCampaignId(Long campaignId, PageableModel pageableModel);

    public Map getById(Long id);

    public Map deliver(Long id);
}
