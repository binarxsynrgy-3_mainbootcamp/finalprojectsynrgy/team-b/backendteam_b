package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.DonationModel;
import com.berbalik.asakarya.dto.PageableModel;

import java.util.Map;

public interface DonationService {

    public Map getAll(Long userId, Long campaignId, PageableModel pageableModel);

    public Map getById(Long id);

    public Map insert(DonationModel donationModel, Long userLoginId);

    public Map update(DonationModel donationModel, Long userLoginId);

    // ADMIN ROLE
    public Map adminGetAll(Long campaignId, PageableModel pageableModel);

    public Map verify(Long id, boolean isVerified);
}
