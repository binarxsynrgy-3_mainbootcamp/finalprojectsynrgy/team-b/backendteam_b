package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.FaqModel;

import java.util.Map;

public interface FaqService {

    public Map getAllByCampaignId(Long campaignId);

    public Map getById(Long id);

    public Map insert(FaqModel faqModel);

    public Map update(FaqModel faqModel);

    public Map delete(Long id);
}
