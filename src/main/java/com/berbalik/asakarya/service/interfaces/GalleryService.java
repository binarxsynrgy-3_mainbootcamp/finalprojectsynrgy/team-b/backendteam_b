package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.GalleryModel;
import com.berbalik.asakarya.dto.PageableModel;

import java.util.Map;

public interface GalleryService {

    public Map getAll(PageableModel pageableModel, Long categoryId, String title);

    public Map getAllByCreatorId(Long creatorId, PageableModel pageableModel);

    public Map getById(Long id);

    public Map insert(GalleryModel galleryModel, Long userLoginId);

    public Map update(GalleryModel galleryModel, Long userLoginId);

    public Map delete(Long id, Long userLoginId);
}
