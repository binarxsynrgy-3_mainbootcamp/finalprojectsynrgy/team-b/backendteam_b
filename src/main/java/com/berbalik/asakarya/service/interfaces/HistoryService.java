package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.HistoryModel;
import com.berbalik.asakarya.dto.PageableModel;

import java.util.Map;

public interface HistoryService {

    public Map getAllByCampaignId(Long campaignId, PageableModel pageableModel);

    public Map getById(Long id);

    public Map insert(HistoryModel historyModel);

    public Map update(HistoryModel historyModel);

    public Map delete(Long id);

    // ADMIN ROLE
    public Map getAll(PageableModel pageableModel);
}
