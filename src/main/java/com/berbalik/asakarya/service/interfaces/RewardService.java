package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.RewardModel;

import java.util.List;
import java.util.Map;

public interface RewardService {

    public Map getAllByCampaignId(Long campaignId);

    public Map getById(Long id);

    public Map insert(List<RewardModel> rewardModels);

    public Map update(List<RewardModel> rewardModels);

    public Map deleteAllByCampaignId(Long id);
}
