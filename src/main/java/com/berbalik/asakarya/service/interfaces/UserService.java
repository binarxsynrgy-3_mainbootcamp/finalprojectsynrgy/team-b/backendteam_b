package com.berbalik.asakarya.service.interfaces;

import com.berbalik.asakarya.dto.PageableModel;
import com.berbalik.asakarya.dto.UpdatePasswordModel;
import com.berbalik.asakarya.dto.UserModel;

import java.util.Map;


public interface UserService {

    public Map register(UserModel userModel, boolean isAdmin);

    public Map login(String baseUrl, UserModel userModel);

    public Map update(UserModel userModel, Long userLoginId);

    public Map getById(Long id);

    public Map delete(Long id);

    public Map updatePassword(UpdatePasswordModel updatePasswordModel, Long userLoginId);

    // ADMIN
    public Map getAll(PageableModel pageableModel);

    public Map resetPassword(UserModel userModel);
}
