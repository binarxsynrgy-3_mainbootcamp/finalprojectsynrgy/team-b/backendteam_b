package com.berbalik.asakarya.service.oauth;

import com.berbalik.asakarya.entity.oauth.User;
import com.berbalik.asakarya.repository.oauth.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class Oauth2UserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findOneByUsername(s);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("Username %s is not found", s));
        }

        return user;
    }

}