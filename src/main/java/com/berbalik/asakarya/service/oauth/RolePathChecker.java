package com.berbalik.asakarya.service.oauth;

import com.berbalik.asakarya.entity.oauth.RolePath;
import com.berbalik.asakarya.repository.oauth.RolePathRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@Slf4j
public class RolePathChecker {

    @Autowired
    private RolePathRepository rolePathRepository;

    public <T extends UserDetails> boolean isAllow(T user, String xUri, String method) {
        if (StringUtils.isEmpty(xUri)) {
            return false;
        }

        List<RolePath> rolePaths = rolePathRepository.findByUser(user);

        if (rolePaths.size() < 1) {
            return false;
        }

        boolean allowed = false;

        try {
            for (RolePath path: rolePaths) {
                log.info("Checking regex: {} {} with string: {} {}", path.getMethod(), path.getPattern(), method, xUri);
                if (xUri.matches(path.getPattern()) && path.getMethod().equalsIgnoreCase(method)) {
                    allowed = true;
                    break;
                }
            }
        } catch (Exception e) {
            log.warn("Error: {}", e.getMessage());
        }

        return allowed;
    }
}
