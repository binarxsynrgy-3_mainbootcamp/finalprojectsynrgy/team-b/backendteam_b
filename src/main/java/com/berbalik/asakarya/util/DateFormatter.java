package com.berbalik.asakarya.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    public static String SimpleDateFormatter() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMyyyyhhmmss");
        return formatter.format(new Date());
    }
}
