package com.berbalik.asakarya.util;

import java.util.Random;

public class DigitGenerator {

    public static int getRandomNumberUsingNextInt(int min, int max) {
        Random random = new Random();
        // The min parameter (the origin) is inclusive, whereas the upper bound max is exclusive.
        return random.nextInt(max - min) + min;
    }
}
