package com.berbalik.asakarya.util;

import com.berbalik.asakarya.config.Config;

import java.util.HashMap;
import java.util.Map;

public class MapResponseTemplate {

    Config config = new Config();

    public Map success(Object obj) {
        Map map = new HashMap();

        map.put("data", obj);
        map.put(config.code, config.succesCode);
        map.put(config.message, config.succesMessage);

        return map;
    }

    public Map created(Object obj) {
        Map map = new HashMap();

        map.put("id", obj);
        map.put(config.code, config.createdCode);
        map.put(config.message, config.createdMessage);

        return map;
    }

    public Map updated() {
        Map map = new HashMap();

        map.put(config.code, config.succesCode);
        map.put(config.message, config.updatedMessage);

        return map;
    }

    public Map deleted() {
        Map map = new HashMap();

        map.put(config.code, config.succesCode);
        map.put(config.message, config.deletedMessage);

        return map;
    }

    public Map notFound(String message) {
        Map map = new HashMap();

        map.put(config.code, config.notFoundCode);
        map.put(config.message, message + " " + config.notFoundMessage);

        return map;
    }

    public Map badRequest() {
        Map map = new HashMap();

        map.put(config.code, config.badRequestCode);
        map.put(config.message, config.badCredentialMessage);

        return map;
    }

    public Map forbidden(String message) {
        Map map = new HashMap();

        map.put(config.code, config.forbiddenCode);
        if (message != null) {
            map.put(config.message, message);
        } else {
            map.put(config.message, config.forbiddenMessage);
        }

        return map;
    }

    public Map internalServerError(String message) {
        Map map = new HashMap();

        map.put(config.code, config.serverErrorCode);
        map.put(config.message, message);

        return map;
    }

}
