package com.berbalik.asakarya.util;

import com.berbalik.asakarya.dto.*;
import com.berbalik.asakarya.entity.*;
import com.berbalik.asakarya.entity.oauth.Role;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class ModelConverter {
    public static List<String> toStringRoleList(List<Role> roles) {
        List<String> stringList = new ArrayList<>();
        for (Role role: roles) {
            stringList.add(role.getName());
        }

        return stringList;
    }

    public static UserModel toUserModel(Profile profile) {

        UserModel userModel = new UserModel();
        userModel.setId(profile.getId());
        userModel.setUsername(profile.getUser().getUsername());
        userModel.setFullName(profile.getFullName());
        userModel.setPhone(profile.getPhone());
        userModel.setGender(profile.getGender());
        userModel.setDob(profile.getDob());
        userModel.setImgUrl(profile.getImgUrl());
        userModel.setCreatedAt(profile.getCreatedAt());
        userModel.setUpdatedAt(profile.getUpdatedAt());
        userModel.setDeletedAt(profile.getDeletedAt());
        userModel.setRoles(toStringRoleList(profile.getUser().getRoles()));

        return userModel;
    }

    public static AddressModel toAddressModel(Address address) {
        AddressModel addressModel = new AddressModel();

        addressModel.setId(address.getId());
        addressModel.setRecipient(address.getRecipient());
        addressModel.setPhone(address.getPhone());
        addressModel.setAddress(address.getAddress());
        addressModel.setPostalCode(address.getPostalCode());
        addressModel.setMain(address.isMain());
        addressModel.setUserId(address.getUser().getId());
        addressModel.setCreatedAt(address.getCreatedAt());
        addressModel.setUpdatedAt(address.getUpdatedAt());
        addressModel.setDeletedAt(address.getDeletedAt());

        return addressModel;
    }

    public static Address toAddress(AddressModel addressModel) {
        Address address = new Address();

        address.setRecipient(addressModel.getRecipient());
        address.setPhone(addressModel.getPhone());
        address.setAddress(addressModel.getAddress());
        address.setPostalCode(addressModel.getPostalCode());
        address.setMain(addressModel.isMain());

        return address;
    }

    public static CreatorModel toCreatorModel(Creator creator) {

        CreatorModel creatorModel = new CreatorModel();
        creatorModel.setId(creator.getId());
        creatorModel.setOrganization(creator.getOrganization());
        creatorModel.setOccupation(creator.getOccupation());
        creatorModel.setSocialMedia(creator.getSocialMedia());
        creatorModel.setSocialMediaAccount(creator.getSocialMediaAccount());
        creatorModel.setBio(creator.getBio());
        creatorModel.setNik(creator.getNik());
        creatorModel.setKtpUrl(creator.getKtpUrl());
        creatorModel.setBankName(creator.getBankName());
        creatorModel.setBankAccount(creator.getBankAccount());
        creatorModel.setBankAccountName(creator.getBankAccountName());
        creatorModel.setCreatedAt(creator.getCreatedAt());
        creatorModel.setUpdatedAt(creator.getUpdatedAt());
        creatorModel.setDeletedAt(creator.getDeletedAt());

        return creatorModel;
    }

    public static CampaignModel toCampaignModel(Campaign campaign){
        CampaignModel campaignModel = new CampaignModel();
        campaignModel.setId(campaign.getId());
        campaignModel.setTitle(campaign.getTitle());
        campaignModel.setDescription(campaign.getDescription());
        campaignModel.setFundAmount(campaign.getFundAmount());
        campaignModel.setDue(campaign.getDue());
        campaignModel.setDaysLeft(ChronoUnit.DAYS.between(LocalDate.now(), campaign.getDue()));
        campaignModel.setLocation(campaign.getLocation());
        campaignModel.setImgUrl(campaign.getImgUrl());
        campaignModel.setWebsite(campaign.getWebsite());
        campaignModel.setSumDonation(campaign.getSumDonation());
        campaignModel.setCountDonation(campaign.getCountDonation());
        campaignModel.setStatus(campaign.getStatus());
        campaignModel.setSumDisbursement(campaign.getSumDisbursement());
        campaignModel.setCategoryId(campaign.getCategory().getId());
        campaignModel.setCategoryName(campaign.getCategory().getCategoryName());
        campaignModel.setCreatorId(campaign.getCreator().getId());
        campaignModel.setCreatorOrganization(campaign.getCreator().getOrganization());
        campaignModel.setProfileFullName(campaign.getCreator().getUser().getProfile().getFullName());
        campaignModel.setProfileImage(campaign.getCreator().getUser().getProfile().getImgUrl());
        campaignModel.setCreatedAt(campaign.getCreatedAt());
        campaignModel.setUpdatedAt(campaign.getUpdatedAt());
        campaignModel.setDeletedAt(campaign.getDeletedAt());

        return campaignModel;
    }

    public static Campaign toCampaign(CampaignModel campaignModel){
        Campaign campaign = new Campaign();

        campaign.setTitle(campaignModel.getTitle());
        campaign.setDescription(campaignModel.getDescription());
        campaign.setFundAmount(campaignModel.getFundAmount());
        campaign.setDue(campaignModel.getDue());
        campaign.setImgUrl(campaignModel.getImgUrl());
        campaign.setWebsite(campaignModel.getWebsite());
        campaign.setLocation(campaignModel.getLocation());
//        campaign.setSumDonation(campaignModel.getSumDonation());
//        campaign.setCountDonation(campaignModel.getCountDonation());
        campaign.setStatus(campaignModel.getStatus());
        campaign.setCreatedAt(campaignModel.getCreatedAt());
        campaign.setUpdatedAt(campaignModel.getUpdatedAt());
        campaign.setDeletedAt(campaignModel.getDeletedAt());

        return campaign;
    }

    public static DonationModel toDonationModel(Donation donation) {
        Long addressId = (donation.getAddress()==null) ? null : donation.getAddress().getId();
        Long userId = (addressId==null) ? null : donation.getAddress().getUser().getId();
        String profileFullName = (addressId==null) ? null : donation.getAddress().getUser().getProfile().getFullName();
        String profileImage = (addressId==null) ? null : donation.getAddress().getUser().getProfile().getImgUrl();

        Long rewardId = (donation.getReward()==null) ? null : donation.getReward().getId();
        Character rewardType = (rewardId==null) ? null : donation.getReward().getRewardType().getType();
        String rewardItem = (rewardId==null) ? null : donation.getReward().getItem();

        DonationModel donationModel = new DonationModel();
        donationModel.setId(donation.getId());
        donationModel.setAmount(donation.getAmount());
//        donationModel.setBankName(donation.getBankName());
//        donationModel.setBankAccount(donation.getBankAccount());
        donationModel.setPaymentSlipUrl(donation.getPaymentSlipUrl());
        donationModel.setNotes(donation.getNotes());
        donationModel.setStatus(donation.getStatus());
        donationModel.setUserId(userId);
        donationModel.setProfileFullName(profileFullName);
        donationModel.setProfileImage(profileImage);
        donationModel.setAddressId(addressId);
        donationModel.setCampaignId(donation.getCampaign().getId());
        donationModel.setCampaignTitle(donation.getCampaign().getTitle());
        donationModel.setCampaignImage(donation.getCampaign().getImgUrl());
        donationModel.setCampaignCategoryName(donation.getCampaign().getCategory().getCategoryName());
        donationModel.setCampaignCreatorName(donation.getCampaign().getCreator().getUser().getProfile().getFullName());
        donationModel.setRewardId(rewardId);
        donationModel.setRewardType(rewardType);
        donationModel.setRewardItem(rewardItem);
        donationModel.setCreatedAt(donation.getCreatedAt());
        donationModel.setUpdatedAt(donation.getUpdatedAt());
        donationModel.setDeletedAt(donation.getDeletedAt());

        return donationModel;
    }

    public static Donation toDonation(DonationModel donationModel) {
        Donation donation = new Donation();
//        donation.setAmount(donationModel.getAmount());
//        donation.setBankName(donationModel.getBankName());
//        donation.setBankAccount(donationModel.getBankAccount());
        donation.setPaymentSlipUrl(donationModel.getPaymentSlipUrl());
        donation.setNotes(donationModel.getNotes());
        donation.setCreatedAt(donationModel.getCreatedAt());
        donation.setUpdatedAt(donationModel.getUpdatedAt());
        donation.setDeletedAt(donationModel.getDeletedAt());

        return donation;
    }

    public static BookmarkModel toBookmarkModel(Bookmark bookmark){
        BookmarkModel bookmarkModel = new BookmarkModel();
        bookmarkModel.setId(bookmark.getId());
        bookmarkModel.setCampaignId(bookmark.getCampaign().getId());
        bookmarkModel.setUserId(bookmark.getUser().getId());
        bookmarkModel.setCreatedAt(bookmark.getCreatedAt());
        bookmarkModel.setUpdatedAt(bookmark.getUpdatedAt());
        bookmarkModel.setDeletedAt(bookmark.getDeletedAt());
        bookmarkModel.setTitle(bookmark.getCampaign().getTitle());
        bookmarkModel.setDescription(bookmark.getCampaign().getDescription());
        bookmarkModel.setFundAmount(bookmark.getCampaign().getFundAmount());
        bookmarkModel.setDue(bookmark.getCampaign().getDue());
        bookmarkModel.setDaysLeft(ChronoUnit.DAYS.between(LocalDate.now(), bookmark.getCampaign().getDue()));
        bookmarkModel.setLocation(bookmark.getCampaign().getLocation());
        bookmarkModel.setImgUrl(bookmark.getCampaign().getImgUrl());
        bookmarkModel.setWebsite(bookmark.getCampaign().getWebsite());
        bookmarkModel.setSumDonation(bookmark.getCampaign().getSumDonation());
        bookmarkModel.setCountDonation(bookmark.getCampaign().getCountDonation());
        bookmarkModel.setStatus(bookmark.getCampaign().getStatus());
        bookmarkModel.setCategoryId(bookmark.getCampaign().getCategory().getId());
        bookmarkModel.setCategoryName(bookmark.getCampaign().getCategory().getCategoryName());
        bookmarkModel.setCreatorId(bookmark.getCampaign().getCreator().getId());
        bookmarkModel.setCreatorOrganization(bookmark.getCampaign().getCreator().getOrganization());
        bookmarkModel.setProfileFullName(bookmark.getCampaign().getCreator().getUser().getProfile().getFullName());
        bookmarkModel.setProfileImage(bookmark.getCampaign().getCreator().getUser().getProfile().getImgUrl());

        return bookmarkModel;
    }

    public static RewardModel toRewardModel(Reward reward) {
        RewardModel rewardModel = new RewardModel();
        rewardModel.setId(reward.getId());
        rewardModel.setCampaignId(reward.getCampaign().getId());
        rewardModel.setRewardTypeId(reward.getRewardType().getId());
        rewardModel.setItem(reward.getItem());
        rewardModel.setLimited(reward.isLimited());
        rewardModel.setImgUrl(reward.getImgUrl());
        rewardModel.setCreatedAt(reward.getCreatedAt());
        rewardModel.setUpdatedAt(reward.getUpdatedAt());
        rewardModel.setDeletedAt(reward.getDeletedAt());

        return rewardModel;
    }

    public static Reward toReward(RewardModel rewardModel) {
        Reward reward = new Reward();
        reward.setItem(rewardModel.getItem());
        reward.setLimited(rewardModel.isLimited());
        reward.setImgUrl(rewardModel.getImgUrl());

        return reward;
    }

    public static ContactModel toContactModel(Contact contact) {
        ContactModel contactModel = new ContactModel();
        contactModel.setId(contact.getId());
        contactModel.setName(contact.getName());
        contactModel.setEmail(contact.getEmail());
        contactModel.setMessage(contact.getMessage());
        contactModel.setCreatedAt(contact.getCreatedAt());
        contactModel.setUpdatedAt(contact.getUpdatedAt());
        contactModel.setDeletedAt(contact.getDeletedAt());

        return contactModel;
    }

    public static HistoryModel toHistoryModel(History history) {
        HistoryModel historyModel = new HistoryModel();

        historyModel.setId(history.getId());
        historyModel.setTitle(history.getTitle());
        historyModel.setActivity(history.getActivity());
        historyModel.setImgUrl(history.getImgUrl());
        historyModel.setCampaignId(history.getCampaign().getId());
        historyModel.setCreatorId(history.getCampaign().getCreator().getId());
        historyModel.setProfileFullName(history.getCampaign().getCreator().getUser().getProfile().getFullName());
        historyModel.setProfileImage(history.getCampaign().getCreator().getUser().getProfile().getImgUrl());
        historyModel.setCreatedAt(history.getCreatedAt());
        historyModel.setUpdatedAt(history.getUpdatedAt());
        historyModel.setDeletedAt(history.getDeletedAt());

        return historyModel;
    }

    public static History toHistory(HistoryModel historyModel) {
        History history = new History();

        history.setTitle(historyModel.getTitle());
        history.setActivity(historyModel.getActivity());
        history.setImgUrl(historyModel.getImgUrl());

        return history;
    }

    public static FaqModel toFaqModel(Faq faq){
        FaqModel faqModel = new FaqModel();

        faqModel.setId(faq.getId());
        faqModel.setQuestion(faq.getQuestion());
        faqModel.setAnswer(faq.getAnswer());
        faqModel.setCampaignId(faq.getCampaign().getId());
        faqModel.setCreatorId(faq.getCampaign().getCreator().getId());
        faqModel.setProfileFullName(faq.getCampaign().getCreator().getUser().getProfile().getFullName());
        faqModel.setProfileImage(faq.getCampaign().getCreator().getUser().getProfile().getImgUrl());
        faqModel.setCreatedAt(faq.getCreatedAt());
        faqModel.setUpdatedAt(faq.getUpdatedAt());
        faqModel.setDeletedAt(faq.getDeletedAt());

        return faqModel;
    }

    public static Faq toFaq(FaqModel faqModel){
        Faq faq = new Faq();

        faq.setQuestion(faqModel.getQuestion());
        faq.setAnswer(faqModel.getAnswer());

        return faq;
    }

    public static DonationRewardModel toDonationRewardModel(Donation donation){

        Long addressId = (donation.getAddress()==null) ? null : donation.getAddress().getId();
        boolean main = (addressId==null) ? null : donation.getAddress().isMain();
        Long userId = (addressId==null) ? null : donation.getAddress().getUser().getId();
        String profileFullName = (addressId==null) ? null : donation.getAddress().getUser().getProfile().getFullName();
        String profileImage = (addressId==null) ? null : donation.getAddress().getUser().getProfile().getImgUrl();

        Long rewardId = (donation.getReward()==null) ? null : donation.getReward().getId();
        Character rewardType = (rewardId==null) ? null : donation.getReward().getRewardType().getType();
        String rewardItem = (rewardId==null) ? null : donation.getReward().getItem();
        String rewardImage = (rewardId==null) ? null : donation.getReward().getImgUrl();

        DonationRewardModel donationRewardModel = new DonationRewardModel();
        donationRewardModel.setId(donation.getId());
        donationRewardModel.setAddressId(addressId);
        donationRewardModel.setRecipient(donation.getAddress().getRecipient());
        donationRewardModel.setPhone(donation.getAddress().getPhone());
        donationRewardModel.setAddress(donation.getAddress().getAddress());
        donationRewardModel.setPostalCode(donation.getAddress().getPostalCode());
        donationRewardModel.setMain(main);
        donationRewardModel.setUserId(userId);
        donationRewardModel.setProfileFullName(profileFullName);
        donationRewardModel.setProfileImage(profileImage);
        donationRewardModel.setCampaignId(donation.getCampaign().getId());
        donationRewardModel.setRewardId(rewardId);
        donationRewardModel.setRewardType(rewardType);
        donationRewardModel.setRewardItem(rewardItem);
        donationRewardModel.setRewardImage(rewardImage);
        donationRewardModel.setDelivered(donation.isDelivered());
        donationRewardModel.setCreatedAt(donation.getCreatedAt());
        donationRewardModel.setUpdatedAt(donation.getUpdatedAt());
        donationRewardModel.setDeletedAt(donation.getDeletedAt());

        return donationRewardModel;
    }

    public static DisbursementModel toDisbursementModel(Disbursement disbursement){
        DisbursementModel disbursementModel = new DisbursementModel();
        disbursementModel.setId(disbursement.getId());
        disbursementModel.setTitle(disbursement.getTitle());
        disbursementModel.setAmount(disbursement.getAmount());
        disbursementModel.setDescription(disbursement.getDescription());
        disbursementModel.setPaymentSlipUrl(disbursement.getPaymentSlipUrl());
        disbursementModel.setUserId(disbursement.getUser().getId());
        disbursementModel.setAdminFullName(disbursement.getUser().getProfile().getFullName());
        disbursementModel.setAdminProfileImage(disbursement.getUser().getProfile().getImgUrl());
        disbursementModel.setCampaignId(disbursement.getCampaign().getId());
        disbursementModel.setCreatedAt(disbursement.getCreatedAt());
        disbursementModel.setUpdatedAt(disbursement.getUpdatedAt());
        disbursementModel.setDeletedAt(disbursement.getDeletedAt());

        return disbursementModel;
    }
    public static Disbursement toDisbursement(DisbursementModel disbursementModel){
        Disbursement disbursement = new Disbursement();

        disbursement.setTitle(disbursementModel.getTitle());
        disbursement.setAmount(disbursementModel.getAmount());
        disbursement.setDescription(disbursementModel.getDescription());
        disbursement.setPaymentSlipUrl(disbursementModel.getPaymentSlipUrl());

        return disbursement;
    }

    public static GalleryModel toGalleryModel(Gallery gallery){
        GalleryModel galleryModel = new GalleryModel();
        galleryModel.setId(gallery.getId());
        galleryModel.setTitle(gallery.getTitle());
        galleryModel.setDescription(gallery.getDescription());
        galleryModel.setWebsite(gallery.getWebsite());
        galleryModel.setImgUrl1(gallery.getImgUrl1());
        galleryModel.setImgUrl2(gallery.getImgUrl2());
        galleryModel.setImgUrl3(gallery.getImgUrl3());
        galleryModel.setPromoted(gallery.isPromoted());
        galleryModel.setCategoryId(gallery.getCategory().getId());
        galleryModel.setCategoryName(gallery.getCategory().getCategoryName());
        galleryModel.setCreatorId(gallery.getCreator().getId());
        galleryModel.setCreatorSocialMedia(gallery.getCreator().getSocialMedia());
        galleryModel.setCreatedAt(gallery.getCreatedAt());
        galleryModel.setUpdatedAt(gallery.getUpdatedAt());
        galleryModel.setDeletedAt(gallery.getDeletedAt());

        return galleryModel;
    }

    public static Gallery toGallery(GalleryModel galleryModel) {
        Gallery gallery = new Gallery();

        gallery.setTitle(galleryModel.getTitle());
        gallery.setDescription(galleryModel.getDescription());
        gallery.setWebsite(galleryModel.getWebsite());
        gallery.setImgUrl1(galleryModel.getImgUrl1());
        gallery.setImgUrl2(galleryModel.getImgUrl2());
        gallery.setImgUrl3(galleryModel.getImgUrl3());

        return gallery;
    }
}
