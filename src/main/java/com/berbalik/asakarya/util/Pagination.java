package com.berbalik.asakarya.util;

import com.berbalik.asakarya.dto.PageableModel;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class Pagination {
    public static Pageable pageable(PageableModel pageableModel) {
        Pageable pageable;
        if ((pageableModel.getSortType().equals("desc")) || (pageableModel.getSortType().equals("descending"))){
            pageable = PageRequest.of(
                    pageableModel.getPage(),
                    pageableModel.getSize(),
                    Sort.by(pageableModel.getSortBy()).descending()
            );
        } else {
            pageable = PageRequest.of(
                    pageableModel.getPage(),
                    pageableModel.getSize(),
                    Sort.by(pageableModel.getSortBy()).ascending()
            );
        }

        return pageable;
    }
}
