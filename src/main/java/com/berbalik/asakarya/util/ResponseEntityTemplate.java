package com.berbalik.asakarya.util;

import com.berbalik.asakarya.config.Config;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public class ResponseEntityTemplate {

    Config config = new Config();

    public ResponseEntity<Map> showResponse(Map obj) {

        HttpStatus status;

        if (obj.get(config.code).equals(config.succesCode)) {
            status = HttpStatus.OK;
        } else if (obj.get(config.code).equals(config.createdCode)) {
            status = HttpStatus.CREATED;
        } else if (obj.get(config.code).equals(config.noContentCode)) {
            status = HttpStatus.NO_CONTENT;
        } else if (obj.get(config.code).equals(config.notFoundCode)) {
            status = HttpStatus.NOT_FOUND;
        } else if (obj.get(config.code).equals(config.forbiddenCode)) {
            status = HttpStatus.FORBIDDEN;
        } else if (obj.get(config.code).equals(config.badRequestCode)) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<Map>(obj, status);
    }

}
