package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.BookmarkModel;
import com.berbalik.asakarya.entity.Bookmark;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageBookmark<T> {
    List<BookmarkModel> content;
    CustomPageable pageable;

    public CustomPageBookmark(Page<Bookmark> page) {
        List<BookmarkModel> bookmarkModels = new ArrayList<>();
        page.getContent().forEach(bookmark -> bookmarkModels.add(ModelConverter.toBookmarkModel(bookmark)));

        this.content = bookmarkModels;
        this.pageable = new CustomPageBookmark.CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
