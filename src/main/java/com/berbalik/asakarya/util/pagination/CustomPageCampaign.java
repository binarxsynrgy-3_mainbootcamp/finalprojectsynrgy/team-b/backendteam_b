package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.CampaignModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageCampaign<T> {
    List<CampaignModel> content;
    CustomPageCampaign.CustomPageable pageable;

    public CustomPageCampaign(Page<Campaign> page) {
        List<CampaignModel> campaignModels = new ArrayList<>();
        page.getContent().forEach(campaign -> campaignModels.add(ModelConverter.toCampaignModel(campaign)));

        this.content = campaignModels;
        this.pageable = new CustomPageCampaign.CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
