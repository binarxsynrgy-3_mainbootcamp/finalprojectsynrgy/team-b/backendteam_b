package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.CreatorModel;
import com.berbalik.asakarya.entity.Creator;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageCreator<T> {
    List<CreatorModel> content;
    CustomPageable pageable;

    public CustomPageCreator(Page<Creator> page) {
        List<CreatorModel> creatorModels = new ArrayList<>();
        page.getContent().forEach(creator -> creatorModels.add(ModelConverter.toCreatorModel(creator)));

        this.content = creatorModels;
        this.pageable = new CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
