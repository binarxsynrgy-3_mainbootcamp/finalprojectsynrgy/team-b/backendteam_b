package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.DisbursementModel;
import com.berbalik.asakarya.dto.DonationModel;
import com.berbalik.asakarya.entity.Disbursement;
import com.berbalik.asakarya.entity.Donation;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageDisbursement<T> {
    List<DisbursementModel> content;
    CustomPageable pageable;

    public CustomPageDisbursement(Page<Disbursement> page) {
        List<DisbursementModel> disbursementModels = new ArrayList<>();
        page.getContent().forEach(disbursement -> disbursementModels.add(ModelConverter.toDisbursementModel(disbursement)));

        this.content = disbursementModels;
        this.pageable = new CustomPageDisbursement.CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
