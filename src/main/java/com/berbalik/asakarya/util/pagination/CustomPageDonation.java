package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.DonationModel;
import com.berbalik.asakarya.entity.Donation;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageDonation<T> {
    List<DonationModel> content;
    CustomPageable pageable;

    public CustomPageDonation(Page<Donation> page) {
        List<DonationModel> donationModels = new ArrayList<>();
        page.getContent().forEach(donation -> donationModels.add(ModelConverter.toDonationModel(donation)));

        this.content = donationModels;
        this.pageable = new CustomPageDonation.CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
