package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.DonationModel;
import com.berbalik.asakarya.dto.DonationRewardModel;
import com.berbalik.asakarya.entity.Donation;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageDonationReward<T> {
    List<DonationRewardModel> content;
    CustomPageDonationReward.CustomPageable pageable;

    public CustomPageDonationReward(Page<Donation> page) {
        List<DonationRewardModel> donationRewardModels = new ArrayList<>();
        page.getContent().forEach(donation -> donationRewardModels.add(ModelConverter.toDonationRewardModel(donation)));

        this.content = donationRewardModels;
        this.pageable = new CustomPageDonationReward.CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
