package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.CampaignModel;
import com.berbalik.asakarya.dto.GalleryModel;
import com.berbalik.asakarya.entity.Campaign;
import com.berbalik.asakarya.entity.Gallery;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageGallery<T> {
    List<GalleryModel> content;
    CustomPageable pageable;

    public CustomPageGallery(Page<Gallery> page) {
        List<GalleryModel> galleryModels = new ArrayList<>();
        page.getContent().forEach(gallery -> galleryModels.add(ModelConverter.toGalleryModel(gallery)));

        this.content = galleryModels;
        this.pageable = new CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
