package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.HistoryModel;
import com.berbalik.asakarya.entity.History;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageHistory<T> {
    List<HistoryModel> content;
    CustomPageable pageable;

    public CustomPageHistory(Page<History> page) {
        List<HistoryModel> historyModels = new ArrayList<>();
        page.getContent().forEach(history -> historyModels.add(ModelConverter.toHistoryModel(history)));

        this.content = historyModels;
        this.pageable = new CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
