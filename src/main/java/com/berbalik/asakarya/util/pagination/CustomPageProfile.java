package com.berbalik.asakarya.util.pagination;

import com.berbalik.asakarya.dto.UserModel;
import com.berbalik.asakarya.entity.Profile;
import com.berbalik.asakarya.util.ModelConverter;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomPageProfile<T> {
    List<UserModel> content;
    CustomPageable pageable;

    public CustomPageProfile(Page<Profile> page) {
        List<UserModel> userModels = new ArrayList<>();
        page.getContent().forEach(profile -> userModels.add(ModelConverter.toUserModel(profile)));

        this.content = userModels;
        this.pageable = new CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(), page.getTotalElements());
    }

    @Data
    class CustomPageable {
        int pageNumber;
        int pageSize;
        long totalElements;

        public CustomPageable(int pageNumber, int pageSize, long totalElements) {

            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.totalElements = totalElements;
        }
    }
}
